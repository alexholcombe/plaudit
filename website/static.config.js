import path from 'path';

const siteData = {
  title: 'Plaudit',
  description: 'Open endorsements from your academic community',
  keywords: [
    'plaudit',
    'orcid',
    'crossref',
    'open access',
    'open science',
    'doi',
  ],
};

const faviconsOptions = {
	inputFile: path.join(__dirname, 'assets/favicon.png'),
	configuration: {
    appName: siteData.title,
    appDescription: siteData.description,
		icons: {
	        favicons: true,
	        // other favicons configuration
		}
	},
};

const siteRoot = (typeof process.env.APP_URL === 'string' && process.env.APP_URL !== 'https://www.plaudit.pub')
  ? process.env.APP_URL
  : 'https://plaudit.pub';

export default {
  entry: path.join(__dirname, 'src', 'index.tsx'),
  siteRoot: siteRoot,
  getSiteData: () => (siteData),
  getRoutes: async () => {
    return [
      {
        path: '/',
        component: 'src/containers/Home',
      },
      {
        path: '/about',
        component: 'src/containers/About',
      },
      {
        path: '/integration',
        component: 'src/containers/Integration',
      },
      {
        path: '/extension',
        component: 'src/containers/Extension',
      },
      {
        path: '/communication',
        component: 'src/containers/Communication',
      },
      {
        path: '/trust',
        component: 'src/containers/Trust',
      },
      {
        path: '/demo',
        component: 'src/containers/Demo',
      },
    ]
  },
  plugins: [
    'react-static-plugin-typescript',
    'react-static-plugin-sass',
    ['react-static-plugin-favicons', faviconsOptions],
  ],
  devServer: {
    port: 8000,
    proxy: {
      '/api': 'http://localhost:5000',
    },
    historyApiFallback: {
      disableDotRule: true
    },
  },
}
