import React from 'react'
import { Head } from 'react-static';
import { OutboundLink } from 'react-ga-donottrack';

const snippet = `<script async src="https://plaudit.pub/embed/endorsements.js"
        data-embedder-id="integrationToken"
></script>`;

export default () => (
  <div>
    <header className="hero is-primary">
      <div className="hero-body">
        <div className="container">
          <Head>
            <title>
              Integration · Plaudit
            </title>
          </Head>
          <h1 className="title">Integration</h1>
          <span className="subtitle">How to add the Plaudit widget to your website</span>
        </div>
      </div>
    </header>
    <section className="section">
      <div className="container">
        <div className="columns">
          <div className="column is-two-thirds content is-medium">
            <p>
              Adding the Plaudit widget to webpages with academic works takes two steps:
              <ol>
                <li>Obtain an integration token</li>
                <li>Insert an HTML snippet to the page</li>
              </ol>
            </p>
            <h2>Obtain an integration token</h2>
            <p>
              To make sure Plaudit can handle the load, we are gradually rolling out integrations.
              Thus, at this point in time it is required
              to <OutboundLink to="mailto:integrate@plaudit.pub" title="Send us an email" eventLabel="integrationContact">contact us</OutboundLink> if
              you are interested in integrating the Plaudit widget into your website. When your
              integration is approved, we will ask you for the host names you want to display the
              widget at (e.g. <code>example.com</code> and <code>dev.example.com</code>). You will
              then receive a token that can be used to integrate the widget at thost host names.
            </p>
            <h2>Insert the integration snippet</h2>
            <p>
              To add the snippet to a page, insert the following HTML:
            </p>
            <pre><code>{snippet}</code></pre>
            <p>
              Make sure to replace <code>integrationToken</code> with the integration token you
              obtained in the previous step.
            </p>
            <p>
              The widget will be placed in the DOM in front of this <code>&lt;script&gt;</code> tag.
              It will automatically detect which work the page is about, provided that its
              DOI is listed in the page's metadata in one of the 
              formats <OutboundLink to="https://www.npmjs.com/package/get-dois#user-content-supported-meta-tags" title="npm package `get-dois` - supported metadata formats" eventLabel="integrationGetDois">supported by get-dois</OutboundLink>.
              (This should cover most commonly-used formats.)
            </p>
          </div>
        </div>
      </div>
    </section>
  </div>
)
