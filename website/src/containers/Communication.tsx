import React from 'react'
import { OutboundLink } from 'react-ga-donottrack';

export default () => (
  <div>
    <header className="hero is-primary">
      <div className="hero-body">
        <div className="container">
          <h1 className="title">Communication resources</h1>
        </div>
      </div>
    </header>
    <section className="section">
      <div className="container">
        <div className="columns">
          <div className="column is-two-thirds content is-medium">
            <h2>Logo and name</h2>
            <p>
              The primary colour of the Plaudit logo is <ColourBadge colour="#0288d1"/>. A scalable vector format can be downloaded below.
              "Plaudit" is spelled with a capitalised P.
            </p>
            <p>
              <OutboundLink
                to="https://gitlab.com/Flockademic/plaudit/raw/master/website/assets/logo.svg"
                className="button is-large"
                eventLabel="communicationLogo"
              >Download Plaudit logo</OutboundLink>
            </p>
            <h2>Social media</h2>
            <p>
              Plaudit is developed by <OutboundLink to="https://twitter.com/Flockademic" title="@Flockademic on Twitter" eventLabel="communicationFlockademicTwitter">@Flockademic</OutboundLink> on Twitter.
            </p>
            <h2>Communication about integrations</h2>
            <p>
              The following example press release can be used as the basis for announcing integration with Plaudit. For custom quotes or joint announcements, <OutboundLink to="mailto:Plaudit@Flockademic.com" title="Send u an email" eventLabel="communicationCustomQuotes">contact us</OutboundLink>.
            </p>
            <h3>Key messages</h3>
            <p>
              [Publisher] integrates <OutboundLink to="https://plaudit.pub" title="Visit the website of Plaudit" eventLabel="communicationPressPlaudit">Plaudit</OutboundLink> to allow its authors get more recognition for their work.
              Through Plaudit, readers can see which of their peers have endorsed a scholarly article, and can easily add their own endorsements.
              Endorsements span multiple publishing platforms and hence provides and independent and trusted signal about the quality of and interest in an article.
            </p>
            <h3>Title: [Publisher] integrates Plaudit for open research endorsements</h3>
            <p className="content is-large">Impact statement: The integration of the independent tool aims to make it easier for researchers to identify interesting content.</p>
            <p>
              [Publisher], in collaboration with <OutboundLink to="https://plaudit.pub" title="Visit the website of Plaudit" eventLabel="communicationPressPlaudit">Plaudit</OutboundLink>, introduces open endorsements for the research it publishes.
              Works published by [Publisher] are now accompanied by a list of academic peers that have endorsed those works, as well as the ability for the reader to add their own endorsement.
            </p>
            <p>
              "We want to make it easier for researchers to identify research that is relevant to them, earlier, and for authors to gain recognition for the work they do," says Plaudit's Vincent Tunru. "By making endorsement data openly available, and being built on open infrastructure like DOI and ORCID, Plaudit provides independently verified public endorsements."
            </p>
            <p>
              [Publisher employee] adds: "We are happy to be able to add an additional signal to inform our readers about the quality of the work we publish. The endorsements verified by Plaudit will help readers better assess which research will be relevant."
            </p>
            <p>
              Other publishers looking to follow the lead of [Publisher] to integrate Plaudit can contact Vincent Tunru at vincent@plaudit.pub.
            </p>
            <p>To find out more about Plaudit, visit <OutboundLink to="https://plaudit.pub" title="Visit the website of Plaudit" eventLabel="communicationPressPlaudit">plaudit.pub</OutboundLink>.</p>
            <p>To find out more about [Publisher], visit [Publisher website].</p>
          </div>
        </div>
      </div>
    </section>
  </div>
);

const ColourBadge: React.SFC<{ colour: string }> = ({ colour }) => (
  <span>
    <i
      aria-hidden={true}
      style={{
        backgroundColor: colour,
        display: 'inline-block',
        width: '1rem',
        height: '1rem',
        borderRadius: '2px',
        marginRight: '0.5rem',
      }}
    />
    <code>{colour}</code>
  </span>
);
