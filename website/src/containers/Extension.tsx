import React from 'react'
import { Head } from 'react-static';
import { OutboundLink } from 'react-ga-donottrack';

import { ExtensionButtons } from '../components/ExtensionButtons';

const trustImage = require('../../assets/extension/trusted.png');
const endorseImage = require('../../assets/extension/endorse.png');
const integratedImage = require('../../assets/extension/integrated.png');

export default () => (
  <div>
    <header className="hero is-medium is-primary">
      <div className="hero-body">
        <div className="container">
          <Head>
            <title>
              Trusted research, faster · Plaudit
            </title>
            <meta name="twitter:card" content="summary" />
            <meta name="twitter:site" content="@PlauditPub" />
            <meta name="twitter:creator" content="@Flockademic" />
            <meta property="og:url" content={process.env.APP_URL + '/extension/'} />
            <meta property="og:title" content="Trusted research, faster — the Plaudit browser extension" />
            <meta property="og:description" content="Open endorsement by the academic community" />
            <meta property="og:image" content={trustImage} />
          </Head>
          <div className="columns">
            <div className="column is-8-widescreen is-offset-2-widescreen">
              <h1 className="title">Plaudit</h1>
              <span className="subtitle">Open endorsements for academic research</span>
              <div style={{ marginTop: '1rem' }}>
                <ExtensionButtons analyticsSuffix="LandingTop" location="header"/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <section id="trusted" className="hero is-light is-medium">
      <div className="hero-body">
        <div className="container">
          <div className="columns">
            <div className="column is-8-widescreen is-offset-2-widescreen">
              <div className="tile is-ancestor">
                <div className="promotionTile is-child">
                  <h2 className="title">Trusted research, faster</h2>
                  <p className="content is-medium">Endorsements help identify reliable research, preprint or not.</p>
                </div>
                <div className="promotionTile is-child">
                  <figure className="extensionScreenshot">
                    <img src={trustImage} alt=""/>
                  </figure>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="integrated" className="hero is-medium">
      <div className="hero-body">
        <div className="container">
          <div className="columns">
            <div className="column is-8-widescreen is-offset-2-widescreen">
              <div className="tile is-ancestor">
                <div className="promotionTile is-child">
                  <h2 className="title">Right where you need it</h2>
                  <p className="content is-medium">
                    Plaudit integrates directly into the most widely used publishers, surfacing endorsements where they're relevant.
                  </p>
                </div>
                <div className="promotionTile is-child">
                  <figure className="extensionScreenshot">
                    <img src={integratedImage} alt=""/>
                  </figure>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="endorse" className="hero is-light is-medium">
      <div className="hero-body">
        <div className="container">
          <div className="columns">
            <div className="column is-8-widescreen is-offset-2-widescreen">
              <div className="tile is-ancestor">
                <div className="promotionTile is-child">
                  <h2 className="title">Share what's good</h2>
                  <p className="content is-medium">
                    Found good research? Let others know, and give the authors the recognition they deserve — even if they cannot afford the publishing fee of a top-tier journal, or are still awaiting peer review.
                  </p>
                </div>
                <div className="promotionTile is-child">
                  <figure className="extensionScreenshot">
                    <img src={endorseImage} alt=""/>
                  </figure>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section className="hero">
      <div className="hero-body">
        <div className="container">
          <div className="columns">
            <div className="column is-8-widescreen is-offset-2-widescreen">
              <h2 className="title">A new way to reward academic excellence</h2>
              <p className="content is-medium">
                Drive the change. Get the extension today
              </p>
              <ExtensionButtons analyticsSuffix="LandingBottom"/>
              <p className="content">
                PS. Are you an innovative publisher of academic
                research? <OutboundLink to="mailto:integrate@plaudit.pub" eventLabel="extensionIntegrationContact">Contact us</OutboundLink> to
                integrate Plaudit directly into your website, and expose endorsements to all visitors.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
)
