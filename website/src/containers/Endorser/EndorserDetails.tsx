import React from 'react'
import { Head } from 'react-static';
import { OutboundLink } from 'react-ga-donottrack';
import { GetEndorserResponse } from '../../../../lib/endpoints';
import { RssIcon } from '../../components/icons';

const orcidIcon = require('../../../assets/icons/orcid.svg');

interface Props {
  endorser: GetEndorserResponse;
}

export default (props: Props) => (
  <div>
    <header className="hero is-primary is-medium">
      <div className="hero-body">
        <div className="container">
          <Head>
            <title>
              Endorsements by {props.endorser.endorser.name} · Plaudit
            </title>
            <link
              rel="alternate"
              type="application/rss+xml"
              title={`Endorsements by ${props.endorser.endorser.name}`}
              href={`${process.env.REACT_APP_URL}/api/endorsers/${props.endorser.endorser.orcid}.rss`}
            />
          </Head>
          <h1 className="title">Endorsements by {props.endorser.endorser.name}</h1>
          <p className="subtitle">
            <OutboundLink
              to={`${process.env.ORCID_URL}${props.endorser.endorser.orcid}`}
              title={`View the ORCID profile of ${props.endorser.endorser.name || props.endorser.endorser.orcid}`}
              eventLabel="orcidProfileSubtitle"
            >
              <img
                src={orcidIcon}
                className="orcidIcon"
                alt=""
              />
              {props.endorser.endorser.orcid}
            </OutboundLink>
          </p>
        </div>
      </div>
    </header>
    <section className="section">
      <div className="container">
        <div className="columns">
          <div className="column is-half-widescreen">
            {renderEndorsements(props.endorser)}
          </div>
          <div className="column is-one-quarter-widescreen is-offset-one-quarter-widescreen">
            <OutboundLink
              to={`${process.env.ORCID_URL}${props.endorser.endorser.orcid}`}
              className="button is-fullwidth"
              title={`View the ORCID profile of ${props.endorser.endorser.name || props.endorser.endorser.orcid}`}
              eventLabel="orcidProfileButton"
            >
              <img
                src={orcidIcon}
                className="orcidIcon"
                alt=""
              />
              View ORCID profile
            </OutboundLink>
          </div>
        </div>
      </div>
    </section>
  </div>
)

function renderEndorsements(data: GetEndorserResponse) {
  if (data.endorser.endorsements.length === 0) {
    return (
      <div>
        <div className="is-clearfix">
          <h2 className="title is-size-4 is-pulled-left">No endorsements</h2>
          <OutboundLink
            to={`${process.env.REACT_APP_URL}/api/endorsers/${data.endorser.orcid}.rss`}
            title={`Subscribe to the RSS feed of ${data.endorser.name || data.endorser.orcid}`}
            className="button is-text is-pulled-right"
            rel="alternate"
            type="application/rss+xml"
            eventLabel="endorserRssButton"
          >
            <RssIcon/>
          </OutboundLink>
        </div>
        <p className="content is-medium">This person has not endorsed any works yet.</p>
      </div>
    );
  }

  const list = data.endorser.endorsements.map((endorsement) => {
    return (
      <li key={`${data.endorser.orcid}-${endorsement.identifierType}:${endorsement.identifier}`}>
        <OutboundLink
          to={endorsement.url}
          title="View this article"
          eventLabel="endorserViewArticle"
        >
          {endorsement.title}
        </OutboundLink>
      </li>
    );
  })

  const title = (data.endorser.endorsements.length === 1)
    ? '1 endorsement:'
    : `${data.endorser.endorsements.length} endorsements:`;

  return (
    <div className="content">
      <div className="is-clearfix">
        <h2 className="title is-size-4 is-pulled-left">{title}</h2>
        <OutboundLink
          to={`${process.env.REACT_APP_URL}/api/endorsers/${data.endorser.orcid}.rss`}
          title={`Subscribe to the RSS feed of ${data.endorser.name || data.endorser.orcid}`}
          className="button is-text is-pulled-right"
          rel="alternate"
          type="application/rss+xml"
          eventLabel="endorserRssButton"
        >
          <RssIcon/>
        </OutboundLink>
      </div>
      <ul>
        {list}
      </ul>
    </div>
  );
}
