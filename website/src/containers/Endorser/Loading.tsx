import React from 'react'
import { Head } from 'react-static';

export default () => (
  <div>
    <header className="hero is-primary is-medium">
      <div className="hero-body">
        <div className="container">
          <Head>
            <title>
              Endorsements · Plaudit
            </title>
          </Head>
          <div className="loadingTitle"/>
          <div className="loadingSubTitle"/>
        </div>
      </div>
    </header>
    <section className="section">
      <div className="container">
        <div className="columns">
          <div className="column is-half-widescreen">
            <div className="loadingHeading"/>
            <div className="loadingArticleTitle"/>
            <div className="loadingArticleTitle"/>
          </div>
          <div className="column is-one-quarter-widescreen is-offset-one-quarter-widescreen">
            <div className="loadingFullwidthButton"/>
          </div>
        </div>
      </div>
    </section>
  </div>
)
