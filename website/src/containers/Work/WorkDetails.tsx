import React from 'react'
import { Head } from 'react-static';
import { OutboundLink } from 'react-ga-donottrack';
import { GetEndorsementsResponse } from '../../../../lib/endpoints';
import { Endorsement } from '../../components/Endorsement';

interface Props {
  endorsements: GetEndorsementsResponse;
}

export default (props: Props) => (
  <div>
    <header className="hero is-primary is-medium">
      <div className="hero-body">
        <div className="container">
          <Head>
            <title>
              Endorsements for "{props.endorsements.workData.title}" · Plaudit
            </title>
          </Head>
          <h1 className="title">
            <OutboundLink
              to={props.endorsements.workData.url}
              title={`View "${props.endorsements.workData.title}"`}
              eventLabel="articleSourceTitle"
            >{props.endorsements.workData.title}</OutboundLink>
          </h1>
          <p className="subtitle">{renderAuthors(props.endorsements.workData.authors)}</p>
        </div>
      </div>
    </header>
    <section className="section">
      <div className="container">
        <div className="columns">
          <div className="column is-half-widescreen">
            {renderEndorsements(props.endorsements)}
          </div>
          <div className="column is-one-quarter-widescreen is-offset-one-quarter-widescreen">
            <OutboundLink
              to={props.endorsements.workData.url}
              className="button is-fullwidth"
              title={`View "${props.endorsements.workData.title}"`}
              eventLabel="articleSource"
            >View this article</OutboundLink>
          </div>
        </div>
      </div>
    </section>
  </div>
)

function renderEndorsements(data: GetEndorsementsResponse) {
  if (data.endorsements.length === 0) {
    return (
      <div>
        <h2 className="title is-size-4">No endorsements</h2>
        <p className="content is-medium">This article has not been endorsed yet.</p>
      </div>
    );
  }

  const list = data.endorsements.map((endorsement) => {
    return (
      <li key={`${endorsement.doi}-${endorsement.orcid}-${endorsement.type}`}>
        <Endorsement endorsement={endorsement}/>
      </li>
    );
  })

  const title = (data.endorsements.length === 1)
    ? '1 endorsement:'
    : `${data.endorsements.length} endorsements:`;

  return (
    <div className="content">
      <h2 className="title is-size-4">{title}</h2>
      <ul>
        {list}
      </ul>
    </div>
  );
}

function renderAuthors(authors: Array<{ name: string; orcid?: string }>) {
  return authors.map((author) => author.name).join(', ');
}
