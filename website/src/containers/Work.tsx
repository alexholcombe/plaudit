import React from 'react'
import { RouteComponentProps } from '@reach/router';

import NotFound from '../pages/404';
import Loading from './Work/Loading';
import WorkDetails from './Work/WorkDetails';
import { GetEndorsementsResponse } from '../../../lib/endpoints';
import { getEndorsements } from '../data/plaudit';

interface OwnProps {}
interface RouteParams {
  /**
   * The PID
   */
  '*': string;
}
export type Props = OwnProps & RouteComponentProps<RouteParams>;
interface State {
  endorsementData?: GetEndorsementsResponse | null;
}

// A DOI is `10.`, followed by a number of digits, then a slash, and then any number of characters
const doiRegex = /^10\.\d{4,}\/.*$/;

export default class extends React.Component<Props, State> {
  state: State = {};

  componentDidMount() {
    this.fetchEndorsements();
  }

  async fetchEndorsements() {
    const [ identifierType, identifier ] = this.props['*']!.split(':');

    if (
      identifierType !== 'doi' ||
      typeof identifier === 'undefined' ||
      !doiRegex.test(identifier)
    ) {
      return;
    }

    try {
      const endorsementData = await getEndorsements(identifier);

      this.setState({ endorsementData: endorsementData });
    } catch (e) {
      this.setState({ endorsementData: null });
    }
  }

  render () {
    const [ identifierType, identifier ] = this.props['*']!.split(':');

    if (
      identifierType !== 'doi' ||
      typeof identifier === 'undefined' ||
      !doiRegex.test(identifier) ||
      this.state.endorsementData === null
    ) {
      return <NotFound/>;
    }

    if (typeof this.state.endorsementData === 'undefined') {
      return <Loading/>;
    }

    return (
      <WorkDetails
        endorsements={this.state.endorsementData}
      />
    );
  }
};
