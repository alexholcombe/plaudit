import React from 'react'
import { Head } from 'react-static';
import { OutboundLink } from 'react-ga-donottrack';
import { Link } from '@reach/router';

export default () => (
  <div>
    <header className="hero is-primary">
      <div className="hero-body">
        <div className="container">
          <Head>
            <title>
              Trust · Plaudit
            </title>
          </Head>
          <h1 className="title">Trust</h1>
          <span className="subtitle">Can I trust Plaudit endorsements?</span>
        </div>
      </div>
    </header>
    <section className="section">
      <div className="container">
        <div className="columns">
          <div className="column is-two-thirds content is-medium">
            <p>
              Research endorsements are signals that can help you determine whether you can trust a
              scholarly article, research data, scientific code, or other academic resources.
              However, trust is a nuanced concept, and ultimately, it is up to you to determine what to
              trust.
            </p>
            <p>
              This page will explain how endorsements through Plaudit work and why they work that way.
              Hopefully, this will help you judge to what extent endorsements can be trusted.
            </p>
            <h2>How easily can endorsements be faked?</h2>
            <p>
              When someone endorses a research object through Plaudit, they first sign in with
              their <OutboundLink to="https://orcid.org/" title="ORCID" eventLabel="trustOrcid">ORCID iD</OutboundLink>.
              Thus, you can be certain that the ORCID iD that is displayed when you hover over an
              endorser's name is actually owned by the endorser.
            </p>
            <h2>Can I trust Plaudit?</h2>
            <p>
              Given that Plaudit is the party that verifies whether an endorser actually owns a given
              ORCID iD, Plaudit theoretically has the ability to fake endorsements. There are two
              measures that mitigate this risk.
            </p>
            <p>
              First, Plaudit is open source. Any one can
              view <OutboundLink to="https://gitlab.com/Flockademic/Plaudit" title="The Plaudit source code" eventLabel="trustCode">the source code</OutboundLink> and,
              given the necessary skills, verify that it actually verifies endorsements.
            </p>
            <p>
              Secondly, Plaudit has no horse in the race.
              The <Link to="/about" title="About Plaudit">goal of Plaudit</Link> is to make research
              openly accessible by removing the pressure to publish in "prestigous"
              journals &mdash; not to promote a particular publication venue, researcher, or research
              object. This can only be achieved if endorsements through Plaudit are a reliable
              alternative, which would be sabotaged if those endorsements are not trustworthy.
            </p>
            <h2>Can I trust endorsers?</h2>
            <p>
              When you encounter research published in a journal you trust, you trust that that
              research has been reviewed and approved of by qualified peers. The same core principle
              holds true for Plaudit. However, instead of having to know and trust journals to find
              trustworthy peers, Plaudit leverages the trustworthiness of those peers directly. In
              other words, it relies on you knowing and trusting the peers whose endorsements are
              listed.
            </p>
            <p>
              That said, any one can register an ORCID iD and enter any name they choose. Therefore,
              if you suspect an endorsement to be fake, be sure to verify that the ORCID iD listed
              for an endorser belongs to the person you expect it to belong to. If you suspect
              a case of identity theft,
              please <OutboundLink
                to="https://support.orcid.org/hc/en-us/articles/360006972493-What-can-I-do-if-I-have-concerns-about-data-in-the-Registry-"
                title="ORCID FAQ:  What can I do if I have concerns about data in the Registry?"
                eventLabel="trustOrcidIdentityTheft"
              >contact ORCID</OutboundLink> about those concerns.
            </p>
            <p>
              Like journal reviews, endorsements through Plaudit run the risk of nepotism.
              Unfortunately, this cannot be fully prevented. However, given that all endorsement
              data on Plaudit is open and transparent, we hope that the scholarly community will be
              able to more effectively combat this than with the traditionally hidden process of
              peer review.
            </p>
          </div>
        </div>
      </div>
    </section>
  </div>
)
