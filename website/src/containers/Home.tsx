import React from 'react'
import { withSiteData, Head } from 'react-static'
import { OutboundLink } from 'react-ga-donottrack';
import { Link } from '@reach/router';

const logo = require('../../assets/logo.svg');
const conceptImage = require('../../assets/concept.svg');
const integrationImage = require('../../assets/embed.png');
const credibilityImage = require('../../assets/credibility.svg');
const openDataImage = require('../../assets/opendata.svg');

export default withSiteData(() => (
  <div>
    <header className="hero is-fullheight is-primary">
      <div className="hero-head"></div>
      <div className="hero-body">
        <div className="container">
          <h1>
            <Head>
              <title>
                Plaudit · Open endorsements from the academic community
              </title>
            </Head>
            <img src={logo} alt="Plaudit" className="landingLogo"/>
          </h1>
            <p className="title">
              Open endorsements from the academic community
            </p>
          <p className="field is-grouped">
            <span className="control">
              <OutboundLink className="button is-large is-black is-inverted" to="#concept" eventLabel="homeLearnMore">Learn more</OutboundLink>
            </span>
            <span className="control">
              <OutboundLink className="button is-large is-white is-outlined" to="mailto:team@plaudit.pub" eventLabel="homeContact">Contact us</OutboundLink>
            </span>
          </p>
        </div>
      </div>
      <div className="hero-foot"></div>
    </header>
    <section id="concept" className="hero is-medium">
      <div className="hero-body">
        <div className="container">
          <div className="columns">
            <div className="column is-8-widescreen is-offset-2-widescreen">
              <div className="tile is-ancestor">
                <div className="promotionTile is-child">
                  <h2 className="title">Light-weight endorsements</h2>
                  <p className="content is-medium">Plaudit links researchers, identified by their <OutboundLink to="https://orcid.org" title="ORCID researcher identifiers" eventLabel="homeIntroOrcid">ORCID</OutboundLink>, to research they endorse, identified by its <OutboundLink to="https://www.doi.org/" title="DOI research identifiers" eventLabel="homeIntroDoi">DOI</OutboundLink>.</p>
                </div>
                <div className="promotionTile is-child">
                  <figure className="image is-3by1">
                    <img src={conceptImage} alt="" style={{ height: '70%', marginTop: '1rem' }}/>
                  </figure>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="integrate" className="hero is-light is-medium">
      <div className="hero-body">
        <div className="container">
          <div className="columns">
            <div className="column is-8-widescreen is-offset-2-widescreen">
              <div className="tile is-ancestor">
                <div className="promotionTile is-child">
                  <h2 className="title">Easy to integrate</h2>
                  <p className="content is-medium">Do you publish research? Your development team or service provider can trivially integrate Plaudit using a simple code snippet.</p>
                </div>
                <div className="promotionTile is-child">
                  <img src={integrationImage} alt=""/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="credibility" className="hero is-medium">
      <div className="hero-body">
        <div className="container">
          <div className="columns">
            <div className="column is-8-widescreen is-offset-2-widescreen">
              <div className="tile is-ancestor">
                <div className="promotionTile is-child">
                  <h2 className="title">Authoritative</h2>
                  <p className="content is-medium">Because endorsements are publisher-independent and provided by known and trusted members of the academic community, they provide credibility for valuable research.</p>
                </div>
                <div className="promotionTile is-child">
                  <figure className="image is-128x128">
                    <img src={credibilityImage} alt="" style={{ margin: '1rem 5rem' }}/>
                  </figure>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="opendata" className="hero is-light is-medium">
      <div className="hero-body">
        <div className="container">
          <div className="columns">
            <div className="column is-8-widescreen is-offset-2-widescreen">
              <div className="tile is-ancestor">
                <div className="promotionTile is-child">
                  <h2 className="title">Open data</h2>
                  <p className="content is-medium">Plaudit is built on open infrastructure. We use permanent identifiers from <OutboundLink to="https://orcid.org" title="ORCID researcher identifiers" eventLabel="homeDataOrcid">ORCID</OutboundLink> and <OutboundLink to="https://www.doi.org/" title="DOI research identifiers" eventLabel="homeDataDoi">DOI</OutboundLink>, and endorsements are fed into <OutboundLink to="https://www.eventdata.crossref.org/guide/sources/experimental/#experimental-source-plaudit" title="CrossRef Event Data documentation on Plaudit" eventLabel="homeDataCrossRef">CrossRef Event Data</OutboundLink>.</p>
                  <p className="content is-medium">We're <OutboundLink to="https://gitlab.com/Flockademic/plaudit/" title="Plaudit source code" eventLabel="homeDataCode">open source</OutboundLink>, community-driven, and not for profit.</p>
                </div>
                <div className="promotionTile is-child">
                  <figure className="image is-3by2">
                    <img src={openDataImage} alt="" style={{ height: '70%', marginTop: '1rem' }}/>
                  </figure>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="getstarted" className="hero is-medium">
      <div className="hero-body">
        <div className="container">
          <div className="columns">
            <div className="column is-8-widescreen is-offset-2-widescreen">
              <h2 className="title">Work with us</h2>
              <p className="content is-large">We're looking for innovators who want to work with us to <Link to="/integration/" title="High-level instructions on how to integrate Plaudit into your platform">integrate</Link> the Plaudit prototype into their preprint server. Is this you?</p>
              <p className="field">
                <span className="control">
                  <OutboundLink className="button is-primary is-large" to="mailto:integrate@plaudit.pub" eventLabel="homeFooterContact" style={{ marginTop: '1rem' }}>Contact us</OutboundLink>
                </span>                   
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
))
