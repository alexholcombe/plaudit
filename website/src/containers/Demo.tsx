import React from 'react'
import { Head } from 'react-static';

export default () => (
  <section className="section">
    <div className="container">
      <Head>
        <title>
          Demo · Plaudit
        </title>
      </Head>
      <h1 className="title">Demo</h1>
      <script async src={`${process.env.APP_URL}/embed/endorsements.js`}
          data-pid="doi:10.7554/elife.36263"
          data-embedder-id="plaudit"
      ></script>
    </div>
  </section>
)
