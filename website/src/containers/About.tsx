import React from 'react'
import { Head } from 'react-static';
import { OutboundLink } from 'react-ga-donottrack';

import './About.scss';

const alfAvatar = require('../../assets/team/alf.jpg');
const joeAvatar = require('../../assets/team/joe.jpg');
const markAvatar = require('../../assets/team/mark.jpg');
const mattiasAvatar = require('../../assets/team/mattias.jpg');
const saraAvatar = require('../../assets/team/sara.jpg');
const vincentAvatar = require('../../assets/team/vincent.jpg');

const Profile = (props: { person: { firstName: string; surName: string; description: string; avatar: string; twitter: string} }) => (
  <div className="media">
    <div className="media-left">
      <figure className="image is-128x128">
        <img src={props.person.avatar} alt={`Picture of ${props.person.firstName}`} className="is-rounded"/>
      </figure>
    </div>
    <div className="media-content">
      <p className="title is-4">{props.person.firstName} {props.person.surName}</p>
      <p className="content">{props.person.description}</p>
      <footer className="content">
        <OutboundLink
          to={`https://twitter.com/${props.person.twitter}`}
          title={`${props.person.firstName} on Twitter`}
          eventLabel="aboutTwitterProfile"
        >
          @{props.person.twitter}
        </OutboundLink>
      </footer>
    </div>
  </div>
);

const Timeline = () => (
  <div className="timeline">
    <header className="timeline-header">
      <span className="tag is-medium is-primary">2018</span>
    </header>
    <div className="timeline-item">
      <div className="timeline-marker"></div>
      <div className="timeline-content">
        <p className="heading">10 May 2018</p>
        <p>Plaudit conceived at the eLife Innovation Sprint</p>
      </div>
    </div>
    <div className="timeline-item">
      <div className="timeline-marker"></div>
      <div className="timeline-content">
        <p className="heading">22 October 2018</p>
        <p>Start development of pilot version supported by eLife, in collaboration with the Center for Open Science.</p>
      </div>
    </div>
    <div className="timeline-header">
      <span className="tag is-medium">&hellip;</span>
    </div>
  </div>
);

export default () => (
  <div>
    <section className="hero is-primary">
      <div className="hero-body">
        <div className="container">
          <Head>
            <title>
              About · Plaudit
            </title>
          </Head>
          <h1 className="title">About Plaudit</h1>
        </div>
      </div>
    </section>
    <section className="section">
      <div className="container">
        <div className="columns">
          <div className="column is-two-thirds">
            <div className="content is-medium">
              <h2>eLife Innovation Sprint 2018</h2>
              <p>Plaudit was conceived of at the <OutboundLink to="https://elifesciences.org/labs/bdd4c9aa/elife-innovation-sprint-2018-project-roundup" title="eLife Innovation Sprint 2018" eventLabel="aboutSprint">eLife Innovation Sprint 2018</OutboundLink>, ran in conjunction with the <OutboundLink to="https://foundation.mozilla.org/opportunity/global-sprint/" title="Mozilla Global Sprint" eventLabel="aboutGlobalSprint">Mozilla Global Sprint 2018</OutboundLink>. During this print, the team set out to tackle a complex problem:</p>
              <blockquote>Can we find a way to recognise the value of scientific content, independent of the journal in which it’s published?</blockquote>
              <p>Solving this problem can help addressing the obsession with publication in "prestigious" journals, and hence stimulate researchers to make their work widely available through platforms like preprint servers and other rapid publication and sharing services.</p>
              <p>The solution the team came up with was Plaudit: a simple mechanism for researchers to endorse valuable scientific work. Not only was this idea realistic in scope, it also nicely aligned incentives:</p>
              <dl>
                <dt>Research consumers</dt>
                <dd>can use endorsements from trusted members of the academic community to show them what research is reliable, interesting and relevant.</dd>
                <dt>Authors</dt>
                <dd>benefit from the credibility endorsements provide their work with.</dd>
                <dt>Preprint servers</dt>
                <dd>gain an independent stamp of crededibility for work they host that deserves it.</dd>
                <dt>Endorsers</dt>
                <dd>can indicate which research is useful to them, and can boost their image of being able to recognise valuable scientific work, much like sitting on an editorial board can.</dd>
              </dl>
              <p>And perhaps most importantly: it fits in with the wider mission of increasing Open Access to publicly funded research.</p>
              <p><OutboundLink to="https://docs.google.com/presentation/d/1tt6fmEbwzIqSXa9R5A7qxl7c7tS6K1ZrhqzATXYNSfQ/" title="Plaudit presentation at the eLife Innovation Sprint" eventLabel="aboutSlides">Slide deck</OutboundLink> · <OutboundLink to="http://sarabosshart.wixsite.com/plauditpub" title="Prototype concept site created at the eLife Innovation Sprint" eventLabel="aboutConceptSite">concept site</OutboundLink></p>

              <h2>Pilot development</h2>
              <p>Between October 22nd, 2018 and February 22nd, 2019, eLife will be supporting us to develop an initial pilot version, and to collaborate with the <OutboundLink to="https://cos.io/" title="Website of the Center for Open Science" eventLabel="aboutCos">Center for Open Science</OutboundLink> to test the concept with researchers.</p>
            </div>
          </div>
          <div className="column"><Timeline/></div>
        </div>
      </div>
    </section>
    <section className="section">
      <div className="container">
        <h2 className="title">The team</h2>
        <div className="tile is-ancestor is-vertical">
          <div className="tile is-parent">
            <div className="tile">
              <Profile person={{ avatar: alfAvatar, firstName: 'Alf', surName: 'Eaton', description: 'Software developer, Manuscripts', twitter: 'invisiblecomma' }}/>
            </div>
            <div className="tile">
              <Profile person={{ avatar: joeAvatar, firstName: 'Joe', surName: 'Wass', description: 'Principal R&D Engineer, Crossref', twitter: 'joewass' }}/>
            </div>
            <div className="tile">
              <Profile person={{ avatar: markAvatar, firstName: 'Mark', surName: 'Patterson', description: 'Executive Director, eLife', twitter: 'marknpatterson' }}/>
            </div>
          </div>
          <div className="tile is-parent">
            <div className="tile">
              <Profile person={{ avatar: mattiasAvatar, firstName: 'Mattias', surName: 'Björnmalm', description: 'Research fellow, Imperial College London', twitter: 'bearore' }}/>
            </div>
            <div className="tile">
              <Profile person={{ avatar: saraAvatar, firstName: 'Sara', surName: 'Bosshart', description: 'Open Access Publisher, IWA Publishing', twitter: 'SeaScientist' }}/>
            </div>
            <div className="tile">
              <Profile person={{ avatar: vincentAvatar, firstName: 'Vincent', surName: 'Tunru', description: 'Software developer, Flockademic', twitter: 'Flockademic' }}/>
            </div>
          </div>
        </div>
      </div>
    </section>
  
  </div>
)
