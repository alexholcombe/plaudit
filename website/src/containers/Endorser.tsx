import React from 'react'
import { RouteComponentProps } from '@reach/router';

import NotFound from '../pages/404';
import Loading from './Endorser/Loading';
import EndorserDetails from './Endorser/EndorserDetails';
import { GetEndorserResponse } from '../../../lib/endpoints';
import { getEndorsements, getEndorser } from '../data/plaudit';

interface OwnProps {}
interface RouteParams {
  orcid: string;
}
export type Props = OwnProps & RouteComponentProps<RouteParams>;
interface State {
  endorserData?: GetEndorserResponse;
}

// An ORCID iD consists of four groups of four digits,
// with the caveat that the final digit can also be an X:
const orcidRegex = /^\d{4}\-\d{4}\-\d{4}\-\d{3}(\d|X)$/;

export default class extends React.Component<Props, State> {
  state: State = {};

  componentDidMount() {
    this.fetchEndorser();
  }

  async fetchEndorser() {
    if (
      typeof this.props.orcid === 'undefined' ||
      !orcidRegex.test(this.props.orcid)
    ) {
      return;
    }

    const endorserData = await getEndorser(this.props.orcid);

    this.setState({ endorserData: endorserData });
  }

  render () {
    if (
      typeof this.props.orcid === 'undefined' ||
      !orcidRegex.test(this.props.orcid)
    ) {
      return <NotFound/>;
    }

    if (typeof this.state.endorserData === 'undefined') {
      return <Loading/>;
    }

    return (
      <EndorserDetails
        endorser={this.state.endorserData}
      />
    );
  }
};
