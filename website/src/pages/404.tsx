import React from 'react'
import { Head } from 'react-static';
import * as ReactGA from 'react-ga-donottrack';
import { globalHistory, Link } from '@reach/router';

// To prevent the 404 page from being prerendered during the build, we only render it after it has
// been mounted (which doesn't happen in the build). We need to prevent it from being prerendered,
// because otherwise it will be shown for dynamic routes (i.e. routes that are not prerendered
// during the build, such as the page that lists the latest endorsements) just before the component
// on that route is rendered.
// See https://github.com/nozzle/react-static/blob/master/examples/non-static-routing/src/containers/404.js
interface State {
  hasMounted: boolean;
}

export default class extends React.Component<{}, State> {
  state = {
    hasMounted: false,
  }
  componentDidMount () {
    this.logMount()
  }
  logMount = () => {
    if (!this.state.hasMounted) {
      this.setState({
        hasMounted: true,
      })
      ReactGA.event({
        category: 'Error',
        action: '404 Not Found',
        label: globalHistory.location.pathname,
        nonInteraction: true,
      });
    }
  }
  render () {
    return this.state.hasMounted ? (
      <div>
        <header className="hero is-warning">
          <div className="hero-body">
            <div className="container">
              <Head>
                <title>
                  Page not found · Plaudit
                </title>
              </Head>
              <h1 className="title">Page not found</h1>
            </div>
          </div>
        </header>
        <section className="section">
          <div className="container content is-large">
            <p>
              Unfortunately, this page could not be found. Perhaps you can find what you're looking for at <Link to="/" title="Plaudit homepage">the homepage</Link>?
            </p>
          </div>
        </section>
      </div>
    ) : null
  }
}
