import React from 'react';
import { Root, Head, withSiteData, Routes as StaticRoutes } from 'react-static';
import { Link, Router, RouteComponentProps } from '@reach/router';
import { hot } from 'react-hot-loader';
import universal from 'react-universal-component';
import { OutboundLink } from 'react-ga-donottrack';

import { AnalyticsListener } from './components/AnalyticsListener';
import './app.scss';
import { Props as EndorserProps } from './containers/Endorser';
import { Props as WorkProps } from './containers/Work';

// Tell TypeScript that we will use React Static's <Routes> element as a
// @reach/router route
const Routes = StaticRoutes as unknown as React.ComponentType<RouteComponentProps>;

interface SiteData {
  title: string;
  description: string;
  keywords: string[];
}

const eLifeLogo = require('../assets/partners/elife.png');
const cosLogo = require('../assets/partners/cos.png');

const Work = universal<WorkProps>(import('./containers/Work'));
const Endorser = universal<EndorserProps>(import('./containers/Endorser'));

const App = withSiteData((props: SiteData) => {
  return (
    <Root>
      <AnalyticsListener>
        <Head>
          <title>{props.title}</title>
          <meta name="description" content={props.description}/>
          <meta name="keywords" content={props.keywords.join(', ')}/>
        </Head>
        <main role="main">
          <Router>
            <Work path="/endorsements/*" />
            <Endorser path="/endorser/:orcid" />
            <Routes default />
          </Router>
        </main>
        <footer className="footer">
          <div className="container">
            <div className="columns">
              <div className="column is-one-third-widescreen is-half-tablet">
                <ul className="Footer__sitemap">
                  <li>
                    <Link to="/" title="Plaudit homepage" className="is-size-4">Plaudit</Link>
                  </li>
                  <li>
                    <Link to="/extension" title="Browser extension">Browser extension</Link>
                  </li>
                  <li>
                    <Link to="/about" title="About Plaudit">About</Link>
                  </li>
                </ul>
              </div>
              <div className="column is-offset-one-third-widescreen">
                <h2 className="title is-size-5 has-text-weight-normal">Partners</h2>
                <OutboundLink
                  to="https://elifesciences.org/labs"
                  title="eLife Innovation"
                  className="Footer__partner"
                  eventLabel="footerElife"
                >
                  <img src={eLifeLogo} alt="eLife"/>
                </OutboundLink>
                <OutboundLink
                  to="https://cos.io/"
                  title="Center for Open Science"
                  className="Footer__partner"
                  eventLabel="footerCos"
                >
                  <img src={cosLogo} alt="Center for Open Science"/>
                </OutboundLink>
              </div>
            </div>
          </div>
        </footer>
      </AnalyticsListener>
    </Root>
  )
});

export default hot(module)(App)
