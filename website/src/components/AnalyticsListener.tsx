import React from 'react';
import * as ReactGA from 'react-ga-donottrack';
import { globalHistory, HistoryListener } from '@reach/router';

const gaDimensions = {
  GA_CONFIG_VERSION: 'dimension1',
  CODE_BRANCH: 'dimension2',
};

ReactGA.initialize(process.env.GA_TRACKING_ID || 'No Tracking ID configured');
ReactGA.set({
  // https://philipwalton.com/articles/the-google-analytics-setup-i-use-on-every-site-i-build/#loading-analyticsjs
  transport: 'beacon',
  // https://philipwalton.com/articles/the-google-analytics-setup-i-use-on-every-site-i-build/#tracking-custom-data
  [gaDimensions.GA_CONFIG_VERSION]: '1',
  [gaDimensions.CODE_BRANCH]: process.env.CODE_BRANCH,
});

export class AnalyticsListener extends React.Component {
  componentDidMount() {
    historyListener({ location: globalHistory.location, action: 'PUSH' });
    globalHistory.listen(historyListener);
  }

  render() {
    return this.props.children;
  }
}

const historyListener: HistoryListener = ({ location }) => {
  ReactGA.set({ page: location.pathname });
  ReactGA.pageview(location.pathname);
};
