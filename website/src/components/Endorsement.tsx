import React from 'react';
import { Link } from '@reach/router';
import { Endorsement as EndorsementType } from '../../../lib/endpoints';

interface Props {
 endorsement: EndorsementType & { userName?: string };
}

export const Endorsement: React.SFC<Props> = (props) => (
  <Link
    to={`/endorser/${props.endorsement.orcid}`}
    title={`View other endorsements by ${props.endorsement.userName || props.endorsement.orcid}`}
    className="content is-medium"
  >
    {props.endorsement.userName || props.endorsement.orcid}
  </Link>
);
