import React from 'react';

interface Props {
  alt?: string;
};

export const RssIcon: React.FC<Props> = (props = { alt: 'Subscribe to feed' }) => {
  const rssIcon = require('material-design-icons/communication/svg/production/ic_rss_feed_48px.svg');

  return (
    <span className="icon">
      <img src={rssIcon} alt={props.alt}/>
    </span>
  );
};
