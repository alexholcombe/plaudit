import React from 'react';
import { detect as detectBrowser } from 'detect-browser';
import { OutboundLink } from 'react-ga-donottrack';

const firefoxLink = 'https://addons.mozilla.org/firefox/addon/plauditpub/';
const chromiumLink = 'https://chrome.google.com/webstore/detail/plaudit/bmiognkendaelpjffodmfimmbcllbcen';
const browserInfo = detectBrowser();

interface Props {
  analyticsSuffix: string;
  location?: 'header' | 'content';
};

export const ExtensionButtons: React.FC<Props> = (props) => {
  // Unfortunately, if the layout with Chrome first is prerendered,
  // then the Chrome link will be present in the Firefox button when the page is loaded in Firefox.
  // Presumably React does not manage to properly reconcile the actual links inside of the
  // <OutboundLink>. This might need a fix here, or in react-ga-donottrack,
  // but for now, we'll make do without browser-detecting trickery:
  return <EqualButtons {...props}/>;
  // if (!browserInfo) {
  //   return <ChromeFirst analyticsSuffix={props.analyticsSuffix}/>;
  // }

  // switch (browserInfo.name) {
  //   case 'firefox':
  //     return <FirefoxFirst {...props}/>;
  //   default:
  //     return <ChromeFirst {...props}/>;
  // }
};

function getPrimaryClass(location: 'header' | 'content' = 'content') {
  return (location === 'header')
    ? 'button is-medium'
    : 'button is-large is-primary';
}
function getSecondaryClass(location: 'header' | 'content' = 'content') {
  return (location === 'header')
    ? 'button is-medium is-primary is-inverted is-outlined'
    : 'button';
}
function getNeutralClass(location: 'header' | 'content' = 'content') {
  return (location === 'header')
    ? 'button is-medium'
    : 'button is-large is-primary';
}

const FirefoxFirst: React.FC<Props> = (props) => (
  <span className="buttons">
    <OutboundLink
      to={firefoxLink}
      eventLabel={`primaryButtonFirefoxExtension${props.analyticsSuffix}`}
      className={getPrimaryClass(props.location)}
    >
      Add to Firefox
    </OutboundLink>
    <OutboundLink
      to={chromiumLink}
      eventLabel={`secondaryButtonChromiumExtension${props.analyticsSuffix}`}
      className={getSecondaryClass(props.location)}
    >
      Add to Chrome
    </OutboundLink>
  </span>
);

const ChromeFirst: React.FC<Props> = (props) => (
  <span className="buttons">
    <OutboundLink
      to={chromiumLink}
      eventLabel={`primaryButtonChromiumExtension${props.analyticsSuffix}`}
      className={getPrimaryClass(props.location)}
    >
      Add to Chrome
    </OutboundLink>
    <OutboundLink
      to={firefoxLink}
      eventLabel={`secondaryButtonFirefoxExtension${props.analyticsSuffix}`}
      className={getSecondaryClass(props.location)}
    >
      Add to Firefox
    </OutboundLink>
  </span>
);

const EqualButtons: React.FC<Props> = (props) => (
  <span className="buttons">
    <OutboundLink
      to={chromiumLink}
      eventLabel={`equalButtonChromiumExtension${props.analyticsSuffix}`}
      className={getNeutralClass(props.location)}
    >
      Add to Chrome
    </OutboundLink>
    <OutboundLink
      to={firefoxLink}
      eventLabel={`equalButtonFirefoxExtension${props.analyticsSuffix}`}
      className={getNeutralClass(props.location)}
    >
      Add to Firefox
    </OutboundLink>
  </span>
);
