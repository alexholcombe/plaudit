import { GetEndorsementsResponse, GetEndorserResponse } from '../../../lib/endpoints';

export const getEndorser = async (orcid: string) => {
  const response = await fetch(
    `/api/endorsers/${orcid}`,
    {
      credentials: 'same-origin',
    },
  );
  const data: GetEndorserResponse = await response.json();

  return data;
};

export const getEndorsements = async (pid: string) => {
  const referrer = encodeURIComponent((document.location || '').toString())
  const response = await fetch(
    `/api/endorsements/${pid}?embedder_id=plaudit&referrer=${referrer}`,
    {
      credentials: 'same-origin',
    },
  );
  const data: GetEndorsementsResponse = await response.json();

  return data;
};
