// Unfortunately, the polyfill cannot be auto-injected with ProvidePlugin, so we manually import it:
// https://github.com/mozilla/webextension-polyfill/issues/156
import browser from 'webextension-polyfill';
import { iframeResizer } from 'iframe-resizer';
import { RequestDoiMessage, ProvideDoiMessage, ExtensionMessage, isProvideDoiMessage } from '../interfaces';

const baseUrl = (typeof process.env.APP_URL === 'string' && process.env.APP_URL !== 'https://www.plaudit.pub')
  ? process.env.APP_URL
  : 'https://plaudit.pub';

document.addEventListener('DOMContentLoaded', async () => {
  const activeTabs = await browser.tabs.query({ active: true });
  const response = await browser.tabs.sendMessage<ExtensionMessage>(
    activeTabs[0].id,
    { type: 'requestDoi' },
  );

  if (typeof response !== 'undefined' && isProvideDoiMessage(response)) {
    insertWidget(response.doi, response.url);
  }
});

function insertWidget(doi: string, url?: string) {
  const container = document.getElementById('container');
  if (container) {
    const iframe = document.createElement('iframe');
    const pid = encodeURIComponent(`doi:${doi}`);
    const referrerArg = (typeof url === 'string')
      ? `&referrer=${encodeURIComponent(url)}`
      : '';
    iframe.src = `${baseUrl}/widget/?pid=${pid}&embedder_id=web_ext&force_auth=true${referrerArg}`;
    iframe.style.border = '0 transparent none';

    container.appendChild(iframe);

    iframeResizer(
      {
        heightCalculationMethod: 'taggedElement',
        checkOrigin: false,
      },
      iframe,
    );
  }
}
