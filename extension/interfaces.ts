export interface FoundDoisMessage {
  type: 'foundDois';
  dois: string[];
}
export interface RequestDoiMessage {
  type: 'requestDoi';
}
export interface ProvideDoiMessage {
  type: 'provideDoi';
  doi: string;
  url?: string;
}

export type ExtensionMessage = FoundDoisMessage | RequestDoiMessage | ProvideDoiMessage;

export function isFoundDoisMessage(message: ExtensionMessage): message is FoundDoisMessage {
  return (message as FoundDoisMessage).type === 'foundDois';
}
export function isRequestDoiMessage(message: ExtensionMessage): message is RequestDoiMessage {
  return (message as RequestDoiMessage).type === 'requestDoi';
}
export function isProvideDoiMessage(message: ExtensionMessage): message is ProvideDoiMessage {
  return (message as ProvideDoiMessage).type === 'provideDoi';
}
