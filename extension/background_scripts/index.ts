// Unfortunately, the polyfill cannot be auto-injected with ProvidePlugin, so we manually import it:
// https://github.com/mozilla/webextension-polyfill/issues/156
import browser from 'webextension-polyfill';
import { ExtensionMessage, isFoundDoisMessage } from '../interfaces';

browser.runtime.onMessage.addListener<ExtensionMessage>((message, sender) => {
  if (isFoundDoisMessage(message)) {
    if (message.dois.length === 1) {
      browser.pageAction.show(sender.tab.id);
    } else {
      browser.pageAction.hide(sender.tab.id);
    }
  }

  return false;
});