const promisify = require ('util').promisify;
const path = require('path');
const fs = require('fs');
const fetch = require('node-fetch');
const { URLSearchParams } = require('url');

const readdir = promisify(fs.readdir);

async function fetchAccessToken(clientId, clientSecret, refreshToken) {
  const postParams = new URLSearchParams();
  postParams.append('client_id', clientId);
  postParams.append('client_secret', clientSecret);
  postParams.append('refresh_token', refreshToken);
  postParams.append('grant_type', 'refresh_token');
  postParams.append('redirect_uri', 'urn:ietf:wg:oauth:2.0:oob');
  const accessTokenResponse = await fetch(
    'https://accounts.google.com/o/oauth2/token',
    {
      method: 'POST',
      body: postParams,
    },
  );
  const accessToken = (await accessTokenResponse.json()).access_token;

  return accessToken;
}

async function uploadExtension(accessToken, appId) {
  const artifactsDir = path.resolve(__dirname, 'web-ext-artifacts/');
  const fileNames = await readdir(artifactsDir);
  const zipPath = path.resolve(artifactsDir, fileNames[0]);
  const zipStream = fs.createReadStream(zipPath);
  const uploadResponse = await fetch(
    `https://www.googleapis.com/upload/chromewebstore/v1.1/items/${appId}`,
    {
      method: 'PUT',
      body: zipStream,
      headers: {
        "Authorization": `Bearer ${accessToken}`,
        "x-goog-api-version": 2,
      },
    },
  );
  const uploadData = await uploadResponse.json();
  console.log('Upload to Chrome Web Store complete, API output:', JSON.stringify(uploadData));
}

async function publishExtension(accessToken, appId) {
  const publishResponse = await fetch(
    `https://www.googleapis.com/chromewebstore/v1.1/items/${appId}/publish`,
    {
      method: 'POST',
      headers: {
        "Authorization": `Bearer ${accessToken}`,
        "x-goog-api-version": 2,
        "Content-Length": 0,
      },
    },
  );
  const publishData = await publishResponse.json();
  console.log('Request to publish extension to the Chrome Web Store submitted. API output:', JSON.stringify(publishData));
}

async function publish() {
  if (
    ![
      'CHROME_WEBSTORE_CLIENT_ID',
      'CHROME_WEBSTORE_CLIENT_SECRET',
      'CHROME_WEBSTORE_API_REFRESH_TOKEN',
      'CHROME_WEBSTORE_APP_ID',
    ].every(key => typeof process.env[key] !== 'undefined')
  ) {
    console.error('Please set your Chrome Webstore credentials through environment variables');
    throw new Error('No Chrome Webstore credentials defined');
  }
  const accessToken = await fetchAccessToken(
    process.env.CHROME_WEBSTORE_CLIENT_ID,
    process.env.CHROME_WEBSTORE_CLIENT_SECRET,
    process.env.CHROME_WEBSTORE_API_REFRESH_TOKEN,
  );

  await uploadExtension(accessToken, process.env.CHROME_WEBSTORE_APP_ID);
  await publishExtension(accessToken, process.env.CHROME_WEBSTORE_APP_ID);
}

publish();
