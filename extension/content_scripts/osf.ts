import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';
import { FoundDoisMessage } from '../interfaces';

export function inject(foundDois: string[]) {
  // On the Open Science Framework, *two* DOIs are sometimes added to the page - the OSF's one,
  // and the one assigned when publishing. Since that means the main script did not tell the popup
  // that there was a DOI on the page, we manually tell the popup about the OSF DOI:
  if (Array.isArray(foundDois) && foundDois.length > 1) {
    const message: FoundDoisMessage = {
      type: 'foundDois',
      dois: [ foundDois[0] ],
    };
    browser.runtime.sendMessage(message);
  }

  detectElementByClassName('col-md-5', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const widget = generateWidget();
  const containerElement = document.createElement('div');
  containerElement.classList.add('p-t-xs');
  containerElement.appendChild(widget);

  const abstractDiv = sidebar.querySelectorAll('.p-t-xs')[0];
  abstractDiv.parentNode!.insertBefore(containerElement, abstractDiv.nextSibling);
}
