import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('sidebar-comments-count', injectPlaudit);
}

function injectPlaudit(commentsSidebar: HTMLElement) {
  const widget = generateWidget();
  const widgetContainer = document.createElement('div');
  widgetContainer.style.borderTop = '1px dashed #ccc';
  widgetContainer.style.paddingBottom = '20px';
  const heading = document.createElement('h2');
  heading.textContent = 'Endorsements';
  heading.classList.add('h2-title');
  // The following font is also used by other smaller sidebar headings:
  heading.style.fontFamily = '"Roboto",sans-serif';
  heading.style.fontWeight = '500';
  widgetContainer.appendChild(heading);

  commentsSidebar.insertAdjacentElement('afterend', widgetContainer);
  widgetContainer.appendChild(widget);
}
