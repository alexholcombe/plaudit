import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('pane-biorxiv-art-tools', injectPlaudit);
}

function injectPlaudit(articleTools: HTMLElement) {
  const widget = generateWidget();
  const container = document.createElement('div');
  container.classList.add('panel-pane');
  const heading = document.createElement('h2');
  heading.textContent = 'Endorsements';
  heading.classList.add('pane-title');
  const content = document.createElement('div');
  content.classList.add('pane-content');
  content.appendChild(widget);
  container.appendChild(heading);
  container.appendChild(content);

  const separator = document.createElement('div');
  separator.classList.add('pane-separator');

  articleTools.insertAdjacentElement('afterend', separator);
  articleTools.insertAdjacentElement('afterend', container);
}
