import { generateWidget } from './generateWidget';
import { detectElementById } from './detectElement';

export function inject(foundDois: string[]) {
  // We add a container to the sidebar that will hold the widget.
  // Since the widget will be empty if the page does not contain a DOI,
  // only add it (and the container) if it will be displayed:
  if (Array.isArray(foundDois) && foundDois.length === 1) {
    detectElementById('bib-sidebar', injectPlaudit);
  }
}

function injectPlaudit(sidebar: HTMLElement) {
  const widget = generateWidget();
  const container = document.createElement('div');
  container.style.borderBottom = 'medium solid #ddd';
  container.appendChild(widget);

  sidebar.insertAdjacentElement('beforebegin', container);
}
