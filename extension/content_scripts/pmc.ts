import { generateWidget } from './generateWidget';
import { detectElementById } from './detectElement';

export function inject(foundDois: string[]) {
  // We add a container to the sidebar that will hold the widget.
  // Since not all PMC articles have a DOI,
  // only add it if it will be displayed:
  if (Array.isArray(foundDois) && foundDois.length === 1) {
    detectElementById('rightcolumn', injectPlaudit);
  }
}

function injectPlaudit(sidebar: HTMLElement) {
  const widget = generateWidget();
  const shareButtons = sidebar.getElementsByClassName('share-buttons')[0];

  shareButtons.insertAdjacentElement('beforebegin', widget);
}
