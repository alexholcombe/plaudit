export interface Pid {
  type: 'doi';
  pid: string;
}

export function generateWidget(pid?: Pid) {
  const widget = document.createElement('script');
  const baseUrl = (typeof process.env.APP_URL === 'string' && process.env.APP_URL !== 'https://www.plaudit.pub')
    ? process.env.APP_URL
    : 'https://plaudit.pub';
  widget.setAttribute('src', `${baseUrl}/embed/endorsements.js`);
  widget.dataset.embedderId = 'web_ext';
  if (typeof pid !== 'undefined') {
    widget.dataset.pid = `${pid.type}:${pid.pid}`;
  }

  return widget;
}
