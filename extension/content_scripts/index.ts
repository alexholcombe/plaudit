// Unfortunately, the polyfill cannot be auto-injected with ProvidePlugin, so we manually import it:
// https://github.com/mozilla/webextension-polyfill/issues/156
import browser from 'webextension-polyfill';
import { getDois } from 'get-dois';
import { inject as injectToArxiv } from './arxiv';
import { inject as injectToOsf } from './osf';
import { inject as injectToEssoar } from './essoar';
import { inject as injectToScipost } from './scipost';
import { inject as injectToF1000research } from './f1000research';
import { inject as injectToNature } from './nature';
import { inject as injectToScienceDirect } from './sciencedirect';
import { inject as injectToWiley } from './wiley';
import { inject as injectToSpringer } from './springer';
import { inject as injectToTandF } from './tandf';
import { inject as injectToOUP } from './oup';
import { inject as injectToBiorXiv } from './biorxiv';
import { inject as injectToPreprintsOrg } from './preprints';
import { inject as injectToZenodo } from './zenodo';
import { inject as injectToMdpi } from './mdpi';
import { inject as injectToPubMedCentral } from './pmc';
import { inject as injectToPnas } from './pnas';
import { FoundDoisMessage, ProvideDoiMessage, isRequestDoiMessage, ExtensionMessage } from '../interfaces';

if (document.location !== null) {
  if (document.location.host === 'osf.io') {
    initialiseOsf();
  } else {
    initialise();
  }
}

browser.runtime.onMessage.addListener<ExtensionMessage>((
  message,
  _sender,
  sendResponse,
) => {
  if (isRequestDoiMessage(message as ExtensionMessage)) {
    const foundDois = getDois(document);

    // Usually, we only want to show the popup when there's just *one* DOI on the page
    // (so as to prevent it from appearing on e.g. listings of multiple DOI's, where it's unclear
    // to which DOI it applies).
    // However, on the Open Science Framework, sometimes their own and the eventual publisher's DOIs
    // are listed - in which case we're fine with endorsing the OSF's DOI:
    if (foundDois.length === 1 || (document.location !== null && document.location.host === 'osf.io')) {
      const response: ProvideDoiMessage = {
        type: 'provideDoi',
        doi: foundDois[0],
        url: (document.location) ? document.location.toString() : undefined,
      };
      sendResponse(response);
    }
  }

  return false;
});

function initialise() {
  const foundDois = getDois(document);

  const message: FoundDoisMessage = {
    type: 'foundDois',
    dois: foundDois,
  };
  browser.runtime.sendMessage(message);
  const includedWidgets = document.querySelectorAll(`script[src="https://plaudit.pub/embed/endorsements.js"],script[src="${process.env.APP_URL}/embed/endorsements.js"]`);
  if (document.location === null || includedWidgets.length > 0 || foundDois.length === 0) {
    // Do nothing; the widget is already included in the current page.
    // This is a safeguard to stop adding the widget through the extension
    // once the platform itself incorporates Plaudit.
    // Alternatively, no DOI could be found on the page,
    // in which case the widget will not work either.
  } else if (document.location.host === 'arxiv.org') {
    injectToArxiv(foundDois);
  } else if (document.location.host === 'osf.io') {
    injectToOsf(foundDois);
  } else if (document.location.host === 'www.essoar.org') {
    injectToEssoar();
  } else if (
    [
      // All these platforms are run by F1000 and have the same DOM structure and similar styles:
      'f1000research.com',
      'wellcomeopenresearch.org',
      'gatesopenresearch.org',
      'mniopenresearch.org',
      'hrbopenresearch.org',
      'aasopenresearch.org',
      'amrcopenresearch.org',
      'isfopenresearch.org',
    ].indexOf(document.location.host) !== -1
  ) {
    injectToF1000research();
  } else if (document.location.host === 'scipost.org') {
    injectToScipost();
  } else if (document.location.host === 'www.nature.com') {
    injectToNature();
  } else if (document.location.host === 'www.sciencedirect.com') {
    injectToScienceDirect();
  } else if (
    // Wiley journals are on onlinelbrary.wiley.com, or one of its subdomains.
    // 'onlinelibrary.wiley.com'.length === 23:
    document.location.host.substring(document.location.host.length - 23) === 'onlinelibrary.wiley.com'
  ) {
    injectToWiley();
  } else if (document.location.host === 'link.springer.com') {
    injectToSpringer();
  } else if (document.location.host === 'www.tandfonline.com') {
    injectToTandF();
  } else if (document.location.host === 'academic.oup.com') {
    injectToOUP();
  } else if (document.location.host === 'www.biorxiv.org') {
    injectToBiorXiv();
  } else if (document.location.host === 'www.preprints.org') {
    injectToPreprintsOrg();
  } else if (document.location.host === 'www.zenodo.org' ||
    document.location.host === 'zenodo.org') {
    injectToZenodo();
  } else if (document.location.host === 'www.mdpi.com' || document.location.host === 'mdpi.com') {
    injectToMdpi();
  } else if (document.location.host === 'www.ncbi.nlm.nih.gov') {
    injectToPubMedCentral(foundDois);
  } else if (document.location.host === 'www.pnas.org') {
    injectToPnas();
  }
}

/**
 * The Open Science Framework has some weird asynchronous loading, where you are presented with a
 * blank page almost instantly, after which the page slowly gets constructed.
 * Since we know DOI metadata is likely to get added to the page at some point in time,
 * we simple delay initialisation until that has been done.
 * (With the cost of also setting up this observer on pages that do not include DOIs.)
 */
function initialiseOsf() {
  const observer = new MutationObserver((mutationList, observer) => {
    const elementMutation = mutationList
      .find((mutation) => (mutation.target as HTMLElement)
      .getAttribute('name') === 'citation_doi');

    if (typeof elementMutation !== 'undefined') {
      observer.disconnect();
      initialise();
    }
  });
  observer.observe(document.head as HTMLElement, { childList: true, subtree: true });
}
