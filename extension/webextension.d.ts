declare module 'webextension-polyfill' {
  export default browser;
}
interface Tab {
  id: number;
  active: boolean;
  hidden: boolean;
  index: number;
  windowId: number;
}

declare namespace browser.pageAction {
  export function show(tabId: number): Promise<void>;
  export function hide(tabId: number): void;
}

declare namespace browser.tabs {
  // The type parameter MessageTypes should be a union type of possible messages
  export function sendMessage<MessageTypes = unknown>(
    tabId: number,
    message: MessageTypes,
    options?: {
      frameId?: number;
    },
  ): Promise<MessageTypes | void>;

  export function query(queryInfo: Partial<{
    active: boolean;
    index: number;
    title: string;
    url: string;
  }>): Promise<Tab[]>;

  export function getCurrent(): Tab;
}

declare namespace browser.runtime {
  // The type parameter MessageTypes should be a union type of possible messages
  export function sendMessage<MessageTypes = unknown>(
    // extensionId?: string, // TypeScript won't let optional arguments precede required arguments
    message: MessageTypes,
    options?: {
      includeTlsChannelId?: boolean,
      toProxyScript?: boolean,
    },
  ): Promise<MessageTypes | void>;
}

declare namespace browser.runtime.onMessage {
  // The type parameter MessageTypes should be a union type of possible messages
  export function addListener<MessageTypes = unknown>(
    callback: (
      message: MessageTypes,
      sender: {
        contextId: number,
        id: string,
        extensionId: string,
        frameId: number,
        url: string,
        tab: Tab,
      },
      sendResponse: (response: MessageTypes) => void,
    ) => boolean | Promise<void>,
  ): void;
}
