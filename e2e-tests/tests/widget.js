describe('The widget', function(){
  it('should open ORCID when clicked', function(){
    browser.url('/widget/?pid=doi:10.1101/318345');

    expect(browser.getTabIds().length).toBe(1);

    // Chrome needs some time before the widget is visible:
    browser.waitForVisible('.Widget__Button');
    browser.element('.Widget__Button').click();

    // After clicking the button to endorse, a new tab should open...
    expect(browser.getTabIds().length).toBe(2);
    browser.switchTab(browser.getTabIds()[1]);
    // ...which will be about:blank for a while, while it loads
    // (note: orcid.org can take a while to load, hence the increased timeout)...
    browser.waitForExist('body > *', 5000);
    // ...the ORCID website:
    expect(browser.getUrl()).toMatch(/https:\/\/(sandbox\.)?orcid.org\//);
  });
});
