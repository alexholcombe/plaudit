describe('The website', function(){
  it('should show a 404 page on an unknown URL, from which you should be able to get back to the home page', function(){
    browser.url('/page-that-does-not-exist/?utm_source=gitlab.com&utm_medium=ci&utm_campaign=integration_tests');

    expect(browser.getUrl()).toMatch('page-that-does-not-exist');

    // The 404 page contents is only shown after the 404 component has mounted,
    // to prevent it from being pre-rendered and then shown while loading dynamic routes.
    // Thus, wait for it to become visible:
    browser.waitForVisible('h1');
    expect(browser.element('h1').getText()).toEqual('Page not found');

    browser.element('.footer a[title="Plaudit homepage"]').click();
    browser.waitForVisible('.hero.is-fullheight');
    
    expect(browser.getUrl()).not.toMatch('page-that-does-not-exist');
  });

  it('should provide integration instructions to those that know the link', function(){
    browser.url('/integration/?utm_source=gitlab.com&utm_medium=ci&utm_campaign=integration_tests');

    expect(browser.isExisting('pre>code')).toBe(true);
  });
});
