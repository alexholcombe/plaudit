const appUrl = process.env.APP_URL || 'https://www.plaudit.pub';

describe('Storybook', function(){
  // The iframe with the PDF takes a while to load, so we need a longer timeout
  let originalTimeout;
  beforeEach(() => {
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout * 2;

    browser.setViewportSize({
      width: 1500,
      height: 1000,
    });
  });
  afterEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });

  it('should allow moving through the flow with existing endorsements', function(){
    browser.url('/storybook/?path=/story/ux-testing--with-existing-endorsements&full=1');

    focusStory();

    // Click the endorsement button
    browser.element('.Widget__Button').click();

    // Login
    fakeOrcidLogin();

    // Provide additional details (mark as robust)
    browser.element('.DetailView__Field input#robust').click();
    browser.element('.DetailView__Submit input[aria-label="Submit your additional remarks"]').click();

    expect(browser.element(`a[href="${appUrl}/endorser/0000-0001-7787-3983"]`).isExisting()).toBe(true);
  });

  it('should allow moving through the flow with the first endorsement', function(){
    browser.url('/storybook/?path=/story/ux-testing--no-endorsements&full=1');

    focusStory();

    // Click the endorsement button
    browser.element('.Widget__Button').click();

    // Login
    fakeOrcidLogin();

    // Provide additional details (mark as robust)
    browser.element('.DetailView__Field input#robust').click();
    browser.element('.DetailView__Submit input[aria-label="Submit your additional remarks"]').click();

    expect(browser.element(`a[href="${appUrl}/endorser/0000-0001-7787-3983"]`).isExisting()).toBe(true);
  });

  it('should allow moving through the flow by keyboard', function(){
    if (browser.desiredCapabilities.browserName === 'firefox') {
      browser.logger.info('Skipping accessibility tests in Firefox, since automated keyboard input does not work: https://github.com/webdriverio/webdriverio/issues/2076');
      return;
    }
    browser.url('/storybook/?path=/story/ux-testing--with-existing-endorsements-logged-in&full=1');

    focusStory();

    // Click the endorsement button
    // Focus the paragraph before the widget, tab onto the Endorsement button, then press it:
    browser.element('aside.meta p').click();
    browser.keys(['Tab', 'Enter']);

    // Provide additional details (mark as robust)
    // Focus the paragraph before the widget. Note that this should usually not be necessary,
    // however, because the widget runs in Storybook here, endorsing resulted in a full page refresh:
    browser.element('aside.meta p').click();
    // Mark as "robust":
    browser.keys(['Tab', 'Space']);
    // Submit the detailed feedback:
    browser.keys(['Enter']);

    expect(browser.element(`a[href="${appUrl}/endorser/0000-0001-7787-3983"]`).isExisting()).toBe(true);
  });
});

function fakeOrcidLogin() {
  browser.waitForVisible('img#orcidLogin');
  browser.element('img#orcidLogin').click();
  browser.waitForVisible('img#orcidPermission');
  browser.element('img#orcidPermission').click();
  browser.waitForExist('body > *');
}

function focusStory() {
  // Give the PDF 3s to have loaded, because it steals the focus when it does.
  browser.pause(3000);
  // Afterwards, we can make sure the frame in general is focused again:
  const storyFrame = browser.element('iframe').value;
  browser.frame(storyFrame);
}
