import { configure, addDecorator, addParameters } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withA11y } from '@storybook/addon-a11y';
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';

// Load all files ending in .js, .jsx, .ts or .tsx, unless the two chars before it are `d.` (i.e. TypeScript typings)
const req = require.context('../src/stories', true, /.*(?!.*\.d).{2}\.(j|t)sx?$/)

addDecorator(withKnobs);
addDecorator(withA11y);

const wideWidgetContainer = {
  name: 'Wide widget container (e.g. OSF Preprints)',
  styles: {
    width: '450px',
    height: '400px',
    padding: '10px',
    backgroundColor: '#ededed',
  },
};
const narrowWidgetContainer = {
  name: 'Narrow widget container (e.g. arXiv)',
  styles: {
    boxSizing: 'content-box',
    width: '230px',
    height: '400px',
    padding: '10px',
    backgroundColor: '#ededed',
  },
};
// Since this config is being parsed in Node, we can rely on Object.values() existing.
// However, since we re-use the tsconfig for the client components, we cannot add Node typings.
// Hence: Object as any.
(Object as any).values(INITIAL_VIEWPORTS).forEach(viewport => {
  // Any styles not set by the default viewport containers will be overridden whenever a custom one
  // is visited. Hence, we have to reset all custom styles for the default containers.
  viewport.styles.backgroundColor = 'transparent';
  viewport.styles.padding = 'inherit';
});
addParameters({
  viewport: {
    viewports: { ...INITIAL_VIEWPORTS, wideWidgetContainer, narrowWidgetContainer },
  },
});

function loadStories() {
  req.keys().forEach((filename) => req(filename));
}

configure(loadStories, module);