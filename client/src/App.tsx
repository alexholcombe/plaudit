import * as QueryString from 'query-string';
import * as React from 'react';

import Widget from './Widget/Widget';

class App extends React.Component {
  public render() {
    const parsedQueryParams = document.location ? QueryString.parse(document.location.search) : {};
    const pid: string | undefined = parsedQueryParams.pid;
    const integratorId: string | undefined = parsedQueryParams.embedder_id;
    const referrer: string | undefined = parsedQueryParams.referrer;
    const enforceAuthentication: boolean = parsedQueryParams.force_auth === 'true';

    if (!pid || pid.substr(0, 'doi:'.length) !== 'doi:') {
      return null;
    }

    return (
      <div className="App">
        <Widget
          doi={pid.substr('doi:'.length)}
          integratorId={integratorId}
          referrer={referrer}
          enforceAuthentication={enforceAuthentication}
        />
      </div>
    );
  }
}

export default App;
