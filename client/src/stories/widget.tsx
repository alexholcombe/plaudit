import { storiesOf, Story, addDecorator } from '@storybook/react';
import * as React from 'react';

import { WidgetView } from '../Widget/View';

const mockEndorsements = [ 
  { endorserName: 'Alice Doe', robust: true },
  { endorserName: 'Bob Doe', robust: true },
  { endorserName: 'Charlie Doe', robust: true },
  { endorserName: 'Dan Doe', robust: true },
 ];
const mockWorkData = {
  title: 'Badges to Acknowledge Open Practices: A Simple, Low-Cost, Effective Method for Increasing Transparency',
  pid: 'doi:10.5438/0012',
};

const wideStories = storiesOf('Widget (wide)', module)
  .addParameters({ viewport: { defaultViewport: 'wideWidgetContainer'} });
addWidgetStories(wideStories);

const narrowStories = storiesOf('Widget (narrow)', module)
  .addParameters({ viewport: { defaultViewport: 'narrowWidgetContainer' } });
addWidgetStories(narrowStories);

function addWidgetStories(stories: Story) {
  stories.add('Without endorsements', () => {
    const endorsements = {};
    const mockHandler = () => ({});

    return (
      <WidgetView
        workData={mockWorkData}
        endorsements={endorsements}
        handleEndorsement={mockHandler}
      />
    );
  });

  stories.add('With one endorsement', () => {
    const endorsements = { 'some-orcid': mockEndorsements[0] };
    const mockHandler = () => ({});

    return (
      <WidgetView
        workData={mockWorkData}
        endorsements={endorsements}
        handleEndorsement={mockHandler}
      />
    );
  });

  stories.add('With two endorsements', () => {
    const endorsements = { 'first-orcid': mockEndorsements[0], 'second-orcid': mockEndorsements[1] };
    const mockHandler = () => ({});

    return (
      <WidgetView
        workData={mockWorkData}
        endorsements={endorsements}
        handleEndorsement={mockHandler}
      />
    );
  });

  stories.add('With three endorsements', () => {
    const endorsements = {
      'first-orcid': mockEndorsements[0],
      'second-orcid': mockEndorsements[1],
      'third-orcid': mockEndorsements[2],
    };
    const mockHandler = () => ({});

    return (
      <WidgetView
        workData={mockWorkData}
        endorsements={endorsements}
        handleEndorsement={mockHandler}
      />
    );
  });

  stories.add('With more than three endorsements', () => {
    const endorsements = {
      'first-orcid': mockEndorsements[0],
      'second-orcid': mockEndorsements[1],
      'third-orcid': mockEndorsements[2],
      // tslint:disable-next-line:object-literal-sort-keys
      'fourth-orcid': mockEndorsements[3],
    };
    const mockHandler = () => ({});

    return (
      <WidgetView
        workData={mockWorkData}
        endorsements={endorsements}
        handleEndorsement={mockHandler}
      />
    );
  }) ;

  stories.add('When the current user has endorsed the work', () => {
    const endorsements = {
      'first-orcid': mockEndorsements[0],
      'second-orcid': mockEndorsements[1],
      'third-orcid': mockEndorsements[2],
    };
    const mockHandler = () => ({});

    return (
      <WidgetView
        workData={mockWorkData}
        endorsements={endorsements}
        handleEndorsement={mockHandler}
        user={{ orcid: 'second-orcid' }}
      />
    );
  });

  stories.add('Providing more detailed feedback', () => {
    const endorsements = {};
    const mockHandler = () => ({});

    return (
      <WidgetView
        workData={mockWorkData}
        endorsements={endorsements}
        handleEndorsement={mockHandler}
        user={{ orcid: 'second-orcid' }}
        setTags={mockHandler}
      />
    );
  });

  stories.add('Having to log in first', () => {
    const endorsements = {};
    const mockHandler = () => ({});

    return (
      <WidgetView
        workData={mockWorkData}
        endorsements={endorsements}
        handleEndorsement={mockHandler}
        handleSignin={mockHandler}
      />
    );
  });
}
