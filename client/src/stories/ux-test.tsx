import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { linkTo } from '@storybook/addon-links'
import LinkTo from '@storybook/addon-links/react'

import { WidgetView } from '../Widget/View';

import osfLogoBlackImg from './assets/osf_logo_black.png';
import osfLogoWhiteImg from './assets/osf_logo_white.png';
import orcidLoginImg from './assets/orcidLogin.png';
import orcidPermissionImg from './assets/orcidPermission.png';
import './assets/styles.css';

const OrcidLogin = () => <img src={orcidLoginImg} id="orcidLogin" style={{margin: '0 auto', display: 'block'}}/>;
const OrcidPermission = () => <img src={orcidPermissionImg} id="orcidPermission" style={{margin: '0 auto', display: 'block'}}/>;

const mockEndorsements = [ 
  { endorserName: 'Alice Doe', robust: true },
  { endorserName: 'Bob Doe', robust: true },
  { endorserName: 'Charlie Doe', robust: true },
  { endorserName: 'Dan Doe', robust: true },
 ];
const mockWorkData = {
  title: 'Badges to Acknowledge Open Practices: A Simple, Low-Cost, Effective Method for Increasing Transparency',
  pid: 'doi:10.5438/0012',
};

const stories = storiesOf('UX testing', module);

const Context = (props: { children: React.ReactNode }) => (
  <div className="mockOsf">
    <aside className="mock-warning">This is a copy of OSFPreprints on behalf of user testing - <a href="https://osf.io/preprints">click here</a> for the real OSF Preprints.</aside>
    <nav>
      <div className="container">
        <img src={osfLogoWhiteImg} alt=""/>
        <div className="service-name">
          <span>OSF</span><span>Preprints</span>
        </div>
      </div>
    </nav>
    <header>
      <div className="container wrapper">
        <div className="title">
          <h1>Badges to Acknowledge Open Practices: A Simple, Low-Cost, Effective Method for Increasing Transparency</h1>
          <ul className="authors">
            <li><a href="https://osf.io/z2u9w">Mallory C. Kidwell</a></li>
            <li><a href="https://osf.io/msrfx">Lili Lazarevic</a></li>
            <li><a href="https://osf.io/gqx2h">Erica Baranski</a></li>
            <li><a href="https://osf.io/i8w73">Tom E Hardwicke</a></li>
            <li><a href="https://osf.io/mdw5y">Sarah Piechowski</a></li>
            <li><a href="https://osf.io/z23d7">Lina-Sophia Falkenberg</a></li>
            <li><a href="https://osf.io/4xtk6">Curtis Kennett</a></li>
            <li><a href="https://osf.io/8mk5n">Agnieszka Slowik</a></li>
            <li><a href="https://osf.io/4n7th">Carina Sonnleitner</a></li>
            <li><a href="https://osf.io/2byr9">Chelsey L. Hess-Holden</a></li>
          </ul>
          <small>
            Created on: Mon Aug 29 2016
            |
            Last edited: Mon Jul 02 2018
          </small>
        </div>
        <div className="logo">
          <img src={osfLogoWhiteImg} alt=""/>
        </div>
      </div>
    </header>
    <article className="container">
      <div className="embed">
        <iframe src="https://mfr.osf.io/render?url=https%3A%2F%2Fosf.io%2Fdownload%2Fjtcuk%2F%3Fdirect%26mode%3Drender" allowFullScreen={true} frameBorder="0" scrolling="yes"></iframe>
      </div>
      <aside className="meta">
        <div className="download">
          <a href="https://osf.io/khbvy/download">Download preprint</a>
          <span className="count">Downloads: 753</span>
        </div>
        <h2>Abstract</h2>
        <p>Beginning January 2014, Psychological Science gave authors the opportunity to signal open data and materials if they qualified for badges that accompanied published articles. Before badges, less than 3% of Psychological Science articles reported open data. After badges, 23% reported open data, with an …</p>
        {props.children}
        <h2>Preprint DOI</h2>
        <p><a href="https://doi.org/10.31219/osf.io/khbvy" className="doi">10.31219/osf.io/khbvy</a></p>
        <h2>Peer-reviewed Publication DOI</h2>
        <p><a href="https://doi.org/10.1371/journal.pbio.1002456" className="doi">10.1371/journal.pbio.1002456</a></p>
        <h2>Disciplines</h2>
        <span className="discipline">Social and Behavioral Sciences</span>
        <span className="discipline">Public Affairs, Public Policy and Public Administration</span>
        <span className="discipline">Science and Technology Policy</span>
        <span className="discipline">Psychology</span>
        <span className="discipline">Library and Information Science</span>
        <span className="discipline">Scholarly Publishing</span>
        <h2>Tags</h2>
        <span className="tag">badges</span>
        <span className="tag">methodology</span>
        <span className="tag">open data</span>
        <span className="tag">open materials</span>
        <span className="tag">open science</span>
        <span className="tag">signals</span>
        <div className="project">
          <div className="logo">
            <img src={osfLogoBlackImg} alt=""/>
          </div>
          <div className="link">
            <p>The project for this preprint is available on the OSF.</p>
            <a href="https://osf.io/bnzx5">Visit project</a>
          </div>
        </div>
      </aside>
    </article>
    <footer>
      <div className="container">
        <h1>OSF</h1>
      </div>
    </footer>
  </div>
);

stories.add('No endorsements', () => {
  const endorsements = {};
  const mockHandler = linkTo('UX testing', 'ORCID login (no endorsements)');

  return (
    <Context><WidgetView
      workData={mockWorkData}
      endorsements={endorsements}
      handleEndorsement={mockHandler}
    /></Context>
  );
});

stories.add('ORCID login (no endorsements)', () => {
  return (
    <LinkTo story="ORCID permission (no endorsements)" >
      <OrcidLogin/>
    </LinkTo>
  );
});

stories.add('ORCID permission (no endorsements)', () => {
  return (
    <LinkTo story="Add details (first endorsement)" >
      <OrcidPermission/>
    </LinkTo>
  );
});

stories.add('Add details (first endorsement)', () => {
  const endorsements = { '0000-0001-7787-3983': mockEndorsements[0] };
  const mockSetTags = linkTo('UX testing', 'Only own endorsement');
  const mockHandleEndorsement = () => undefined;

  return (
    <Context>
      <WidgetView
        workData={mockWorkData}
        endorsements={endorsements}
        handleEndorsement={mockHandleEndorsement}
        user={{ orcid: '0000-0001-7787-3983' }}
        setTags={mockSetTags}
        justEndorsed={true}
      />
    </Context>
  );
});

stories.add('Only own endorsement', () => {
  const endorsements = { '0000-0001-7787-3983': mockEndorsements[0] };
  const mockHandler = () => undefined;

  return (
    <Context>
      <WidgetView
        workData={mockWorkData}
        endorsements={endorsements}
        handleEndorsement={mockHandler}
        user={{ orcid: '0000-0001-7787-3983' }}
        justEndorsed={true}
      />
    </Context>
  );
});

stories.add('With existing endorsements', () => {
  const endorsements = { '0000-0002-4510-2233': mockEndorsements[1] };
  const mockHandler = linkTo('UX testing', 'ORCID login (with endorsements)');

  return (
    <Context>
      <WidgetView
        workData={mockWorkData}
        endorsements={endorsements}
        handleEndorsement={mockHandler}
        user={{ orcid: '0000-0001-7787-3983' }}
      />
    </Context>
  );
});

stories.add('ORCID login (with endorsements)', () => {
  return (
    <LinkTo story="ORCID permission (with endorsements)" >
      <OrcidLogin/>
    </LinkTo>
  );
});

stories.add('ORCID permission (with endorsements)', () => {
  return (
    <LinkTo story="Add details (second endorsement)" >
      <OrcidPermission/>
    </LinkTo>
  );
});

stories.add('Add details (second endorsement)', () => {
  const endorsements = { '0000-0001-7787-3983': mockEndorsements[0], '0000-0002-4510-2233': mockEndorsements[1] };
  const mockSetTags = linkTo('UX testing', 'With own and existing endorsements');
  const mockHandleEndorsement = () => undefined;

  return (
    <Context>
      <WidgetView
        workData={mockWorkData}
        endorsements={endorsements}
        handleEndorsement={mockHandleEndorsement}
        user={{ orcid: '0000-0001-7787-3983' }}
        setTags={mockSetTags}
        justEndorsed={true}
      />
    </Context>
  );
});

stories.add('With own and existing endorsements', () => {
  const endorsements = { '0000-0001-7787-3983': mockEndorsements[0], '0000-0002-4510-2233': mockEndorsements[1] };
  const mockHandler = () => undefined;

  return (
    <Context>
      <WidgetView
        workData={mockWorkData}
        endorsements={endorsements}
        handleEndorsement={mockHandler}
        user={{ orcid: '0000-0001-7787-3983' }}
        justEndorsed={true}
      />
    </Context>
  );
});

stories.add('With existing endorsements, logged in', () => {
  const endorsements = { '0000-0002-4510-2233': mockEndorsements[1] };
  const mockHandler = linkTo('UX testing', 'Add details (second endorsement, logged in)');

  return (
    <Context>
      <WidgetView
        workData={mockWorkData}
        endorsements={endorsements}
        handleEndorsement={mockHandler}
        user={{ orcid: '0000-0001-7787-3983' }}
      />
    </Context>
  );
});

stories.add('Add details (second endorsement, logged in)', () => {
  const endorsements = { '0000-0001-7787-3983': mockEndorsements[0], '0000-0002-4510-2233': mockEndorsements[1] };
  const mockSetTags = linkTo('UX testing', 'With own and existing endorsements, logged in');
  const mockHandleEndorsement = () => undefined;

  return (
    <Context>
      <WidgetView
        workData={mockWorkData}
        endorsements={endorsements}
        handleEndorsement={mockHandleEndorsement}
        user={{ orcid: '0000-0001-7787-3983' }}
        setTags={mockSetTags}
        justEndorsed={true}
      />
    </Context>
  );
});

stories.add('With own and existing endorsements, logged in', () => {
  const endorsements = { '0000-0001-7787-3983': mockEndorsements[0], '0000-0002-4510-2233': mockEndorsements[1] };
  const mockHandler = () => undefined;

  return (
    <Context>
      <WidgetView
        workData={mockWorkData}
        endorsements={endorsements}
        handleEndorsement={mockHandler}
        user={{ orcid: '0000-0001-7787-3983' }}
        justEndorsed={true}
      />
    </Context>
  );
});
