import './DetailView.scss';

import * as React from 'react';

interface Props {
  setTags: (tags: string[]) => void;
};
interface State {
  tags: string[];
};

export class DetailView extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      tags: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.toggleRobust = this.toggleRobust.bind(this);
    this.toggleClear = this.toggleClear.bind(this);
    this.toggleExciting = this.toggleExciting.bind(this);
  }

  public render() {
    return (
      <aside className="DetailView">
        <form id="endorsementDetails" onSubmit={this.handleSubmit} className="DetailView__Form">
          {/* .DetailView__Tags would intuitively be a <fieldset> and span#tagLegend a <legend>, */}
          {/* but unfortunately those cannot be a flex container. */}
          {/* See https://stackoverflow.com/a/35466231 */}
          <span className="DetailView__Tags">
            <span id="tagLegend" className="DetailView__TagLine">
              Endorsed! Mark as&hellip;
            </span>
            <div role="group" aria-labelledby="tagLegend" className="DetailView__TagOptions">
              <label htmlFor="robust" className="DetailView__Field">
                <input
                  type="checkbox"
                  name="robust"
                  id="robust"
                  checked={this.state.tags.indexOf('robust') !== -1}
                  onChange={this.toggleRobust}
                />
                <span>robust</span>
              </label>
              <label htmlFor="clear" className="DetailView__Field">
                <input
                  type="checkbox"
                  name="clear"
                  id="clear"
                  checked={this.state.tags.indexOf('clear') !== -1}
                  onChange={this.toggleClear}
                />
                <span>clear</span>
              </label>
              <label htmlFor="exciting" className="DetailView__Field">
                <input
                  type="checkbox"
                  name="exciting"
                  id="exciting"
                  checked={this.state.tags.indexOf('exciting') !== -1}
                  onChange={this.toggleExciting}
                />
                <span>exciting</span>
              </label>
            </div>
          </span>
          <div className="DetailView__Submit" >
            <input
              type="submit"
              value="✔"
              aria-label="Submit your additional remarks"
              form="endorsementDetails"
            />
          </div>
        </form>
      </aside>
    );
  }

  handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    this.props.setTags(this.state.tags);
  }

  toggleRobust() {
    return this.toggleTag('robust');
  }

  toggleClear() {
    return this.toggleTag('clear');
  }

  toggleExciting() {
    return this.toggleTag('exciting');
  }

  toggleTag(tag: string) {
    this.setState(prevState => {
      const tagIndex = prevState.tags.indexOf(tag);
      if (tagIndex === -1) {
        return {
          tags: prevState.tags.concat(tag),
        };
      }

      return {
        tags: prevState.tags.slice(0, tagIndex)
          .concat(prevState.tags.slice(tagIndex + 1)),
      };
    });
  }
}
