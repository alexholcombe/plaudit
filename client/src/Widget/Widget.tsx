import 'iframe-resizer/js/iframeResizer.contentWindow.min.js';
import * as React from 'react';

import { ReactComponent as Logo } from './logo.svg';
import { Endorsement, GetEndorsementsResponse, PostEndorsementsResponse } from '../../../lib/endpoints';
import { WidgetView } from './View';

interface EndorsementsByOrcid { [ orcid: string ]: { endorserName?: string; robust?: boolean; } }
interface IProps {
  doi: string;
  /** Ask the user to authenticate before showing the endorsement button. Used in the extension toolbar button. */
  enforceAuthentication: boolean;
  integratorId?: string;
  referrer?: string;
};
interface IState {
  user?: {
    orcid: string;
    name?: string;
  };
  endorsements?: EndorsementsByOrcid;
  workData?: { title: string, pid: string };
  // Used to add a class to the widget to animate the logo:
  justEndorsed: boolean;
};

class Widget extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      justEndorsed: false,
    };

    this.handleEndorsement = this.handleEndorsement.bind(this);
    this.handleSignin = this.handleSignin.bind(this);
  }

  public componentDidMount() {
    this.fetchEndorsementData();
  }

  public async fetchEndorsementData() {
    const queryParams: { [key: string]: string | undefined } = {
      embedder_id: this.props.integratorId,
      referrer: this.props.referrer,
    };
    const queryString = Object.keys(queryParams)
      .filter(key => typeof queryParams[key] !== 'undefined')
      .map(key => `${key}=${encodeURIComponent(queryParams[key]!)}`).join('&');

    const response = await fetch(
      `/api/endorsements/${this.props.doi}?${queryString}`,
      {
        credentials: 'same-origin',
      },
    );
    const data: GetEndorsementsResponse = await response.json();

    const endorsementsByOrcid = mapEndorsementsToOrcids(data.endorsements);

    this.setState({
      endorsements: endorsementsByOrcid,
      workData: { title: data.workData.title, pid: `doi:${this.props.doi}` },
      user: data.user,
    });
  }

  public render() {
    if (!this.state.endorsements || !this.state.workData) {
      // Don't render the widget while we're still loading existing endorsements...
      return null;
    }

    // When `handleSignin` is `undefined`, the login link is not even shown if the user is not logged in
    // (We usually prompt for sign-in after endorsing - just not in the extension.)
    const handleSignin = (this.props.enforceAuthentication && typeof this.state.user === 'undefined')
      ? this.handleSignin
      : undefined;

    return (
      <WidgetView
        endorsements={this.state.endorsements}
        handleEndorsement={this.handleEndorsement}
        handleSignin={handleSignin}
        user={this.state.user}
        workData={this.state.workData}
        justEndorsed={this.state.justEndorsed}
      />
    );
  }

  private async handleSignin(event: React.FormEvent) {
    event.preventDefault();

    if (typeof this.state.user !== 'undefined') {
      return;
    }

    await this.authenticate();

    // After authenticating, re-fetching endorsement data should include user details:
    await this.fetchEndorsementData();
  }

  private async handleEndorsement() {
    if (!this.state.user) {
      await this.authenticate();
    }

    return this.endorse();
  }

  private authenticate() {
    const authPromise = new Promise((resolve) => {
      const authWindow = window.open('/api/orcid');

      if (!authWindow) {
        return;
      }

      // This is somewhat hackish, however:
      //  - We can't listen to the `unload` event, because that is fired as soon as we redirect to
      //    orcid.org.
      //  - We can't listen to the `close` event because the window API does not actually fire that:
      //    https://developer.mozilla.org/en-US/docs/Web/Events/close
      // While it is somewhat of a workaround, it is unlikely to stop working, unless the `.closed`
      // property gets removed - which I wouldn't expect to happen, given that it is the recommended
      // way to check for whether a window is open on MDN:
      // https://developer.mozilla.org/en-US/docs/Web/API/Window/open#FAQ
      const timer = setInterval(() => {
        if (authWindow.closed) {
          clearInterval(timer);

          // When the window is closed, the user has hopefully authenticated,
          // in which cases future requests to the server will include that user's details.
          resolve();
        }
      }, 200);
    });

    return authPromise;
  }

  private async endorse() {
    this.setState({ justEndorsed: true });
    const response = await fetch(
      `/api/endorsements/${this.props.doi}`,
      {
        body: JSON.stringify({ type: 'robust' }),
        credentials: 'same-origin',
        headers: new Headers({
          'Content-Type': 'application/json',
        }),
        method: 'POST',
      },
    );

    const data: PostEndorsementsResponse = await response.json();

    this.setState({ endorsements: mapEndorsementsToOrcids(data.endorsements), user: data.user });
  }
}

export default Widget;

function mapEndorsementsToOrcids(
  endorsements: Array<Endorsement & { userName?: string }>,
): EndorsementsByOrcid {
  const endorsementsByOrcid = endorsements.reduce<EndorsementsByOrcid>(
    (soFar, endorsement) => {
      soFar[endorsement.orcid] = soFar[endorsement.orcid] || {};
      soFar[endorsement.orcid].endorserName = endorsement.userName;
      soFar[endorsement.orcid][endorsement.type] = true;

      return soFar;
    },
    {},
  );

  return endorsementsByOrcid;
}
