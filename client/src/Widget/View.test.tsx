import React from 'react';
import renderer from 'react-test-renderer';
import { WidgetView } from './View';

const mockWork = {
  pid: 'arbitrary:pid',
  title: 'Arbitrary title',
};
const mockUser = {
  orcid: 'arbitrary-orcid',
  name: 'Arbitrary name',
};

describe('The view, when not logged in,', () => {
  it('should render consistently when asked to log in first', () => {
    const component = renderer.create(
      <WidgetView
        workData={mockWork}
        endorsements={{}}
        handleEndorsement={() => undefined}
        handleSignin={() => undefined}
      />,
    );

    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should render consistently without endorsements', () => {
    const component = renderer.create(
      <WidgetView
        workData={mockWork}
        endorsements={{}}
        handleEndorsement={() => undefined}
      />,
    );
  
    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should render consistently with one endorsement', () => {
    const mockEndorsements = {
      'first-orcid': {
        endorserName: 'First endorser',
      },
    };
    const component = renderer.create(
      <WidgetView
        workData={mockWork}
        endorsements={mockEndorsements}
        handleEndorsement={() => undefined}
      />,
    );
  
    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should render consistently with two endorsements', () => {
    const mockEndorsements = {
      'first-orcid': {
        endorserName: 'First endorser',
      },
      'second-orcid': {
        endorserName: 'Second endorser',
      },
    };
    const component = renderer.create(
      <WidgetView
        workData={mockWork}
        endorsements={mockEndorsements}
        handleEndorsement={() => undefined}
      />,
    );
  
    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should render consistently when endorsements overflow', () => {
    const mockEndorsements = {
      'first-orcid': {
        endorserName: 'First endorser',
      },
      'second-orcid': {
        endorserName: 'Second endorser',
      },
      'third-orcid': {
        endorserName: 'Third endorser',
      },
    };
    const component = renderer.create(
      <WidgetView
        workData={mockWork}
        endorsements={mockEndorsements}
        handleEndorsement={() => undefined}
      />,
    );
  
    expect(component.toJSON()).toMatchSnapshot();
  });
});

describe('The view, when the user is logged in', () => {
  describe('and has not endorsed the work,', () => {
    it('should render consistently without endorsements', () => {
      const component = renderer.create(
        <WidgetView
          user={mockUser}
          workData={mockWork}
          endorsements={{}}
          handleEndorsement={() => undefined}
        />,
      );
    
      expect(component.toJSON()).toMatchSnapshot();
    });
  
    it('should render consistently with one endorsement', () => {
      const mockEndorsements = {
        'first-orcid': {
          endorserName: 'First endorser',
        },
      };
      const component = renderer.create(
        <WidgetView
          user={mockUser}
          workData={mockWork}
          endorsements={mockEndorsements}
          handleEndorsement={() => undefined}
        />,
      );
    
      expect(component.toJSON()).toMatchSnapshot();
    });
  
    it('should render consistently with two endorsements', () => {
      const mockEndorsements = {
        'first-orcid': {
          endorserName: 'First endorser',
        },
        'second-orcid': {
          endorserName: 'Second endorser',
        },
      };
      const component = renderer.create(
        <WidgetView
          user={mockUser}
          workData={mockWork}
          endorsements={mockEndorsements}
          handleEndorsement={() => undefined}
        />,
      );
    
      expect(component.toJSON()).toMatchSnapshot();
    });
  
    it('should render consistently when endorsements overflow', () => {
      const mockEndorsements = {
        'first-orcid': {
          endorserName: 'First endorser',
        },
        'second-orcid': {
          endorserName: 'Second endorser',
        },
        'third-orcid': {
          endorserName: 'Third endorser',
        },
      };
      const component = renderer.create(
        <WidgetView
          user={mockUser}
          workData={mockWork}
          endorsements={mockEndorsements}
          handleEndorsement={() => undefined}
        />,
      );
    
      expect(component.toJSON()).toMatchSnapshot();
    });
  });

  describe('and has endorsed the work,', () => {
    it('should display the detail view when a method to handle that was passed', () => {
      const component = renderer.create(
        <WidgetView
          user={mockUser}
          workData={mockWork}
          endorsements={{}}
          handleEndorsement={() => undefined}
          setTags={() => undefined}
        />,
      );

      expect(component.toJSON()).toMatchSnapshot();
    });

    it('should render consistently with one endorsement', () => {
      const mockEndorsements = {
        [mockUser.orcid]: {
          endorserName: mockUser.name,
        },
      };
      const component = renderer.create(
        <WidgetView
          user={mockUser}
          workData={mockWork}
          endorsements={mockEndorsements}
          handleEndorsement={() => undefined}
        />,
      );
    
      expect(component.toJSON()).toMatchSnapshot();
    });
  
    it('should render consistently with two endorsements', () => {
      const mockEndorsements = {
        'first-orcid': {
          endorserName: 'First endorser',
        },
        [mockUser.orcid]: {
          endorserName: mockUser.name,
        },
      };
      const component = renderer.create(
        <WidgetView
          user={mockUser}
          workData={mockWork}
          endorsements={mockEndorsements}
          handleEndorsement={() => undefined}
        />,
      );
    
      expect(component.toJSON()).toMatchSnapshot();
    });
  
    it('should render consistently when endorsements overflow', () => {
      const mockEndorsements = {
        'first-orcid': {
          endorserName: 'First endorser',
        },
        [mockUser.orcid]: {
          endorserName: mockUser.name,
        },
        'third-orcid': {
          endorserName: 'Third endorser',
        },
      };
      const component = renderer.create(
        <WidgetView
          user={mockUser}
          workData={mockWork}
          endorsements={mockEndorsements}
          handleEndorsement={() => undefined}
        />,
      );
    
      expect(component.toJSON()).toMatchSnapshot();
    });
  });
});
