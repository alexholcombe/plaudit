import './Widget.scss';

import * as React from 'react';

import { DetailView } from './DetailView';
import { ReactComponent as Logo } from './logo.svg';

interface WorkData {
  pid: string;
  title: string;
};
interface EndorsementsByOrcid { [ orcid: string ]: { endorserName?: string; robust?: boolean; } }
interface Props {
  user?: {
    orcid: string;
    name?: string;
  };
  endorsements: EndorsementsByOrcid;
  workData: WorkData;
  handleEndorsement: () => void;
  setTags?: (tags: string[]) => void;
  // The containing component can flip this to true to trigger an "applause" animation
  justEndorsed?: boolean;
  /** When this function is provided and the user is not logged in, they will be asked to do so: */
  handleSignin?: (event: React.FormEvent) => void;
};

const appUrl = process.env.STORYBOOK_APP_URL || process.env.REACT_APP_URL || 'https://plaudit.pub';

export class WidgetView extends React.PureComponent<Props> {
  render() {
    if (this.props.handleSignin && typeof this.props.user === 'undefined') {
      return renderSignInModal(this.props.handleSignin);
    }

    if (typeof this.props.setTags === 'function') {
      return <DetailView setTags={this.props.setTags}/>;
    }

    if (Object.keys(this.props.endorsements).length === 0) {
      return renderNoEndorsements(this.handleEndorsement.bind(this), this.props.workData);
    }

    return renderWithEndorsements(
      this.props.endorsements,
      this.handleEndorsement.bind(this),
      this.props.workData,
      this.props.user && this.props.user.orcid,
      this.props.justEndorsed,
    );
  }

  // Handles activation of the button element - either through clicking or by keypress.
  // See https://www.w3.org/TR/html5/single-page.html#activation
  handleEndorsement(event: React.MouseEvent<HTMLButtonElement>) {
    event.preventDefault();

    return this.props.handleEndorsement();
  }
}

const renderSignInModal = (
  handleSignin: (event: React.FormEvent) => void,
) => (
  <form onSubmit={handleSignin} className="Widget" data-iframe-height="data-iframe-height">
    <span className="Widget__FlexContainer">
      <span className="Widget__Brand">
        <Logo role="img" aria-label="" className="Widget__logo"/>
      </span>
      <span className="Widget__Tagline">
        <span className="Widget__TaglineContent">
          Please <button type="submit" className="Widget__SignInButton">sign in to ORCID</button> to endorse this work.
        </span>
      </span>
    </span>
  </form>
);

const renderNoEndorsements = (
  handleEndorsement: (event: React.MouseEvent<HTMLButtonElement>) => void,
  workData: WorkData,
) => (
  <section>
    <button
      onClick={handleEndorsement}
      tabIndex={0}
      title={`Endorse "${workData.title}"`}
      className="Widget Widget--no_endorsements"
      data-iframe-height="data-iframe-height"
    >
      {/* This is needed because <button> elements cannot be flex containers. */}
      {/* See https://stackoverflow.com/a/35466231 */}
      <span className="Widget__FlexContainer">
        <span className="Widget__ButtonContainer">
          <span className="Widget__Button">
            <Logo role="img" aria-label="" className="Widget__logo"/>
          </span>
        </span>
        <span className="Widget__Tagline">
          <span className="Widget__TaglineContent">
            Be the first to endorse <Work data={workData}/>
          </span>
        </span>
      </span>
    </button>
  </section>
);

const renderUser = (
  userOrcid: string,
  endorsements: EndorsementsByOrcid,
) => {
  const endorserName = endorsements[userOrcid].endorserName || userOrcid;
  return (
    <a
      target="_blank"
      href={`${appUrl}/endorser/${userOrcid}`}
      className="Widget__Endorser"
      title={`View other endorsements by you (${endorserName})`}
    >
      You
    </a>
  );
};

const renderEndorser = (
  endorserOrcid: string,
  endorsements: EndorsementsByOrcid,
) => {
  const endorserName = endorsements[endorserOrcid].endorserName || endorserOrcid;
  return (
    <a
      target="_blank"
      href={`${appUrl}/endorser/${endorserOrcid}`}
      className="Widget__Endorser"
      title={`View other endorsements by ${endorserName}`}
    >
      {endorserName}
    </a>
  );
};

const renderTagline = (
  endorsements: EndorsementsByOrcid,
  workData: WorkData,
  userOrcid?: string,
) => {
  const orcids = Object.keys(endorsements);

  // If the current user has endorsed this work, show that first:
  if (typeof userOrcid !== 'undefined' && typeof endorsements[userOrcid] !== 'undefined') {
    if (orcids.length === 1) {
      return (
        <>
          {renderUser(userOrcid, endorsements)} have endorsed <Work data={workData}/>.
        </>
      );
    }

    const otherEndorsers = orcids.filter(endorserOrcid => endorserOrcid !== userOrcid);
    if (orcids.length === 2) {
      return (
        <>
          {renderUser(userOrcid, endorsements)} and {renderEndorser(otherEndorsers[0], endorsements)} have endorsed <Work data={workData}/>.
        </>
      );
    }

    return (
      <>
        {renderUser(userOrcid, endorsements)} and <a
          target="_blank"
          href={`${appUrl}/endorsements/${workData.pid}`}
          title="View all endorsements"
          className="Widget__Endorser"
        >{otherEndorsers.length} others</a> have endorsed <Work data={workData}/>.
      </>
    );
  }

  // If the current user has not endorsed this work, list other endorsements:
  const latestEndorser = orcids[orcids.length - 1];
  if (orcids.length === 1) {
    return <>{renderEndorser(latestEndorser, endorsements)} has endorsed <Work data={workData}/>.</>;
  }
  if (orcids.length === 2) {
    return <>{renderEndorser(latestEndorser, endorsements)} and {renderEndorser(orcids[0], endorsements)} have endorsed <Work data={workData}/>.</>;
  }

  const otherEndorsers = orcids.slice(0, -1);
  return (
    <>{renderEndorser(latestEndorser, endorsements)} and <a
      target="_blank"
      href={`${appUrl}/endorsements/${workData.pid}`}
      title="View all endorsements"
      className="Widget__Endorser"
    >{otherEndorsers.length} others</a> have endorsed <Work data={workData}/>.</>
  );
};

const renderWithEndorsements = (
  endorsements: EndorsementsByOrcid,
  handleEndorsement: (event: React.MouseEvent<HTMLButtonElement>) => void,
  workData: WorkData,
  userOrcid?: string,
  justEndorsed?: boolean,
) => {
  const tagline = renderTagline(endorsements, workData, userOrcid);
  const buttonOrBrand = (typeof userOrcid !== 'undefined' && Object.keys(endorsements).includes(userOrcid))
    ? (
      <span className="Widget__Brand">
        <Logo role="img" aria-label="Endorse" className="Widget__logo"/>
      </span>
    )
    : (
      <span className="Widget__ButtonContainer">
        <button
          onClick={handleEndorsement}
          tabIndex={0}
          title={`Endorse "${workData.title}"`}
          className="Widget__Button"
        >
          <Logo role="img" aria-label="" className="Widget__logo"/>
        </button>
      </span>
    );

  const className = (justEndorsed === true) ? 'Widget Widget--just-endorsed' : 'Widget';
  return (
    <section className={className} data-iframe-height="data-iframe-height">
      {/* When there are no endorsements, the <section> above is a <button>. */}
      {/* That requires the .Widget__FlexContainer as a workaround. */}
      {/* We also include that here to be able to apply consistent styling. */}
      <span className="Widget__FlexContainer">
        {buttonOrBrand}
        <span className="Widget__Tagline">
          <span className="Widget__TaglineContent">
            {tagline}
          </span>
        </span>
      </span>
    </section>
  );
}

const Work: React.SFC<{ data: WorkData }> = (props) => (
  <abbr className="WorkDetail" title={props.data.title}>this work</abbr>
);
