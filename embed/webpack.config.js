const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: {
    endorsements: path.resolve(__dirname, 'endorsements.ts'),
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name].js'
  },
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: [".ts", ".tsx", ".js"]
  },
  plugins: [
    new webpack.EnvironmentPlugin({
      APP_URL: 'https://plaudit.pub',
    }),
  ],
  module: {
    rules: [
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      { test: /\.tsx?$/, loader: "ts-loader" }
    ]
  },
};
