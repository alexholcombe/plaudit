variable "aws_account_id" {}
variable "aws_access_key_id" {}
variable "aws_secret_access_key" {}
variable "heroku_email" {}
variable "heroku_api_key" {}
variable "cloudflare_email" {}
variable "cloudflare_token" {}
variable "orcid_client_id" {}
variable "orcid_client_secret" {}
variable "orcid_url" {}
variable "crossref_client_id" {}
variable "crossref_client_secret" {}
variable "crossref_url" {}
variable "session_secret" {}
variable "ga_tracking_id" {}
variable "domain" {
  default = "plaudit.pub"
}

data "template_file" "subdomain" {
  template = "${terraform.workspace == "master" ? "$${production}" : "$${review}"}"

  vars {
    production = "www"
    review = "${terraform.workspace}-review"
  }
}

terraform {
  backend "s3" {
    bucket = "terraform-remote-state-plaudit"
    key = "terraform.tfstate"
    encrypt = true
  }
}

provider "heroku" {
  version = "~> 1.0"

  email = "${var.heroku_email}"
  api_key = "${var.heroku_api_key}"
}

provider "cloudflare" {
  version = "~> 1.0"

  email = "${var.cloudflare_email}"
  token = "${var.cloudflare_token}"
}

resource "heroku_app" "heroku_app" {
  name = "p5t-${terraform.workspace}"
  region = "eu"
  config_vars {
    # Needed to also install devDependencies during build:
    YARN_PRODUCTION = "false"
    NPM_PRODUCTION = "false"
    # Used by the app:
    APP_URL = "https://${data.template_file.subdomain.rendered}.${var.domain}"
    REACT_APP_URL = "https://${data.template_file.subdomain.rendered}.${var.domain}"
    STORYBOOK_APP_URL = "https://${data.template_file.subdomain.rendered}.${var.domain}"
    ORCID_CLIENT_ID = "${var.orcid_client_id}"
    ORCID_CLIENT_SECRET = "${var.orcid_client_secret}"
    ORCID_URL = "${var.orcid_url}"
    CROSSREF_CLIENT_ID = "${var.crossref_client_id}"
    CROSSREF_CLIENT_SECRET = "${var.crossref_client_secret}"
    CROSSREF_URL = "${var.crossref_url}"
    SESSION_SECRET = "${var.session_secret}"
    GA_TRACKING_ID = "${var.ga_tracking_id}"
    CODE_BRANCH = "${terraform.workspace}"
  }
  buildpacks = [
    "heroku/nodejs"
  ]
}

resource "heroku_addon" "database" {
  app  = "${heroku_app.heroku_app.name}"
  plan = "heroku-postgresql:hobby-dev"
}

resource "heroku_domain" "plaudit_heroku_domain" {
  app      = "${heroku_app.heroku_app.name}"
  hostname = "${cloudflare_record.plaudit_subdomain_record.hostname}"
}

resource "heroku_domain" "plaudit_heroku_root_domain" {
  count = "${data.template_file.subdomain.rendered == "www" ? 1 : 0}"
  app      = "${heroku_app.heroku_app.name}"
  hostname = "${cloudflare_record.plaudit_root_record.hostname}"
}


output "heroku_git_url" {
  value = "${heroku_app.heroku_app.git_url}"
}
output "heroku_hostname" {
  value = "${heroku_app.heroku_app.heroku_hostname}"
}

resource "cloudflare_record" "plaudit_subdomain_record" {
  domain  = "${var.domain}"
  name    = "${data.template_file.subdomain.rendered}"
  value   = "${heroku_app.heroku_app.heroku_hostname}"
  type    = "CNAME"
  ttl     = 1 # 1: automatic
  proxied = true
}

resource "cloudflare_record" "plaudit_root_record" {
  count = "${data.template_file.subdomain.rendered == "www" ? 1 : 0}"
  domain = "plaudit.pub"
  name   = "plaudit.pub"
  value  = "${heroku_app.heroku_app.heroku_hostname}"
  type   = "CNAME"
  ttl    = 1
  proxied = true
}

# Note: these settings apply not just to this branch, but to everything on plaudit.pub!
# That's why it's only active when working on the `www` subdomain (i.e. production).
resource "cloudflare_zone_settings_override" "plaudit_cloudflare_settings" {
  count = "${data.template_file.subdomain.rendered == "www" ? 1 : 0}"
  name = "${cloudflare_record.plaudit_subdomain_record.domain}"

  settings {
    tls_1_3 = "on"
    always_use_https = "on"
    ssl = "full"
    # Make sure Tor users do not have to enter a CAPTCHA every time:
    security_level = "essentially_off"
  }
}

# Note: these settings apply not just to this branch, but to everything on plaudit.pub!
# That's why it's only active when working on the `www` subdomain (i.e. production).
resource "cloudflare_page_rule" "plaudit_cloudflare_page_rules_no-www" {
  count = "${data.template_file.subdomain.rendered == "www" ? 1 : 0}"
  zone = "${cloudflare_record.plaudit_subdomain_record.domain}"
  target = "*www.${cloudflare_record.plaudit_subdomain_record.domain}/*"
  priority = 10

  actions = {
    forwarding_url = {
      url = "https://${cloudflare_record.plaudit_subdomain_record.domain}/$2",
      status_code = 301
    }
  }
}

# Note: these settings apply not just to this branch, but to everything on plaudit.pub!
# That's why it's only active when working on the `www` subdomain (i.e. production).
resource "cloudflare_page_rule" "plaudit_cloudflare_page_rules_always-https" {
  count = "${data.template_file.subdomain.rendered == "www" ? 1 : 0}"
  zone = "${cloudflare_record.plaudit_subdomain_record.domain}"
  target = "http://*${cloudflare_record.plaudit_subdomain_record.domain}/*"
  priority = 20

  actions = {
    # With the always_use_https action present, CloudFlare does not allow addition actions:
    always_use_https = true,
  }
}


output "live_url" {
  value = "https://${data.template_file.subdomain.rendered}.${var.domain}"
}

