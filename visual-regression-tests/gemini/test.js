// NOTE: Try not to use spaces in suite names.
//       It prevents screenshots from loading properly in the HTML report in GitLab CI/CD.

captureWidget('wide');
captureWidget('narrow');

function captureWidget(view) {
    gemini.suite(`no-endorsements-${view}`, (suite) => {
      suite.setUrl(`?path=/story/widget-${view}--without-endorsements`)
          .setCaptureElements('#storybook-preview-iframe')
          .capture('Storybook');
    })

    gemini.suite(`one-endorsement-${view}`, (suite) => {
      suite.setUrl(`?path=/story/widget-${view}--with-one-endorsement`)
          .setCaptureElements('#storybook-preview-iframe')
          .capture('Storybook');
    })

    gemini.suite(`two-endorsements-${view}`, (suite) => {
      suite.setUrl(`?path=/story/widget-${view}--with-two-endorsements`)
          .setCaptureElements('#storybook-preview-iframe')
          .capture('Storybook');
    })

    gemini.suite(`three-endorsements-${view}`, (suite) => {
      suite.setUrl(`?path=/story/widget-${view}--with-three-endorsements`)
          .setCaptureElements('#storybook-preview-iframe')
          .capture('Storybook');
    })

    gemini.suite(`more-than-three-endorsements-${view}`, (suite) => {
      suite.setUrl(`?path=/story/widget-${view}--with-more-than-three-endorsements`)
          .setCaptureElements('#storybook-preview-iframe')
          .capture('Storybook');
    })

    gemini.suite(`current-user-${view}`, (suite) => {
      suite.setUrl(`?path=/story/widget-${view}--when-the-current-user-has-endorsed-the-work`)
          .setCaptureElements('#storybook-preview-iframe')
          .capture('Storybook');
    })

    gemini.suite(`detail-view-${view}`, (suite) => {
      suite.setUrl(`?path=/story/widget-${view}--providing-more-detailed-feedback`)
          .setCaptureElements('#storybook-preview-iframe')
          .capture('Storybook');
    })

    gemini.suite(`forced-login-${view}`, (suite) => {
      suite.setUrl(`?path=/story/widget-${view}--having-to-log-in-first`)
          .setCaptureElements('#storybook-preview-iframe')
          .capture('Storybook');
    })
}
