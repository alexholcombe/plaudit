const appUrl = process.env.APP_URL || 'https://plaudit.pub';

module.exports = {
  rootUrl: `${appUrl}/storybook/`,
  gridUrl: 'http://selenium:4444/wd/hub',

  browsers: {
    // Unfortunately we're running into errors running Firefox in CI...
    chrome: {
      desiredCapabilities: {
        browserName: 'chrome'
      }
    },
  },

  compareOpts: {
    stopOnFirstFail: false,
  },
  compositeImage: true,
  screenshotsDir: './snapshots',

  system: {
    plugins: {
      'html-reporter/gemini': {
        enabled: true,
        path: 'gemini-report',
        defaultView: 'all',
      },
      'gemini-junit-reporter': {
        reportPath: 'junit.xml',
      },
    }
  },
};
