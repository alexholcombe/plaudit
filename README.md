Plaudit: open endorsements by the academic community.

Plaudit is a simple, light-weight mechanism that allows an individual with an ORCID to endorse an object with a DOI.

This repository is the codebase behind Plaudit. To run the application locally, make sure [Docker Compose](https://docs.docker.com/compose/) is installed. Then, in the root directory, run `docker-compose up`. You can then view the application at http://localhost:3000.

# Application structure

The application consist of a server and a client, both written in Javascript (or more specifically: [TypeScript](https://www.typescriptlang.org/)).

We use [Yarn](https://yarnpkg.com/) for dependency management, splitting up the application in several packages:

- `plaudit-server`, located in `/server`: the code for the API.
- `plaudit-client`, located in `/client`: the widget front-end.
- `plaudit`, located in `/`: contains the glue code to link the two together.
- `plaudit-website`, located in `/website`: Plaudit's online presence.
- `plaudit-extension`, located in `/extension`: a WebExtension that injects the Plaudit widget into preprint servers.
- `plaudit-embed`, located in `/embed`: the script that integrators can incorporate into their website to add the widget.

## `plaudit-server`

A simple [Express](https://expressjs.com/) server, connecting to the [PostgreSQL](https://www.postgresql.org/) database using [Sequelize](http://docs.sequelizejs.com/) as ORM. This handles authentication and the storage of endorsements. When deployed, this also serves the client.

To prevent you having to set up and initialise a local PostgreSQL database, this project includes a Docker image that does so for you.

## `plaudit-client`

A small [React](https://reactjs.org/) app. Its components can also be shown statically using mocked data. To do so, make sure all dependencies are up-to-date, then run `yarn run storybook` in the `/client` folder. The components should now be visible on http://localhost:9001.

## `plaudit`

This glues the server and the client together. When running locally (usually within Docker), this project provides the `yarn run watch` command that:

- starts the server in watch mode, i.e. it reloads when you change the server code. This server is available on http://localhost:5000.
- starts a separate server that serves the client in watch mode, and also proxies requests to endpoints starting with `/api` to the back-end server. This one is available at http://localhost:3000.
- starts a separate server that servers the website in watch mode, and also proxies requests to endpoints starting with `/api` to the back-end server. This one is available at http://localhost:8000.
- starts a separate server that servers the Storybook of the widget components. This one is available at http://localhost:9001.

It also allows building all Plaudit modules into a server package through `yarn run build`, the output of which will be placed in `/dist`. This built server can then -provided the required environment variables are set- be run through `yarn start`. For the production application, this is done on [Heroku](https://www.heroku.com/).

Deployment is done by GitLab CI/CD when new commits are pushed.

## `plaudit-website`

This is relatively independent of the other packages, although it is served up by `plaudit-server` as well in production. To run it locally, run `yarn start` from the `website` directory, after running `yarn` to install dependencies if needed.

The website runs on [React Static](https://react-static.js.org/), using [Bulma](https://bulma.io/) for styling.

It can also display the endorsements for a given PID, and in the future will likely display more dynamic data. When testing locally, it can only fetch that data when the back-end server is also running. See the `plaudit` heading above for instructions on how to start them together.

## `plaudit-extension`

This, too, is independent of the other packages. It is also not served by the server, but instead generates a `.xpi` file that people can install. When the version number in manifest.json on the `master` branch is not yet published, it will automatically be published on push.

The extension at this point is not that full-featured, and not yet intended to be used by users. Rather, it serves as a way to easily test the widget at various supported websites, and through the browser chrome on websites that list a DOI.

To test the extension locally, run `yarn start` from the `extension` directory, after running `yarn` to install dependencies if needed. A new Firefox window with a clean profile will then be opened, with the extension loaded.

## `plaudit-embed`

Whereas `plaudit-client` is isolated from the host website through an iframe (preventing e.g. CSS from being overridden), integrators use a `<script>` tag to add Plaudit to their website. This provides us with flexibility should the integration method change, and allows us to do things client-side like detecting the DOI embedded in the host page.

`plaudit-embed` is that script. Hence, it is important to minimise its bundle size.
