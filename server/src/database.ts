import 'reflect-metadata';
import { createConnection } from 'typeorm';

export async function initialiseDatabase() {
  try {
    const connection = await createConnection();

    await connection.runMigrations({
      transaction: true,
    });

    console.log('Initialised DB');

    return connection;
  } catch (e) {
    console.error('Datbase error:', e);

    return e;
  }
}
