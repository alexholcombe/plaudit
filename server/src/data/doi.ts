import fetch from 'node-fetch';

// See https://citeproc-js.readthedocs.io/en/latest/csl-json/markup.html#date-fields
export type CiteProcDate =
  // [year, month, day of month]:
  [ [ number, number, number ] ] |
  // [start year, start month, day of start month], [end year, end month, day of end month]:
  [ [ number, number, number], [ number, number, number ] ]
;

// See https://citeproc-js.readthedocs.io/en/latest/csl-json/markup.html#name-fields
export interface CiteProcPerson {
  family: string;
  given?: string;
  suffix?: string;
  'non-dropping-particle'?: string;
  /**
   * The property `ORCID` is supported by at least CrossRef, but not mentioned in the CiteProc docs.
   * Even when supported, it might not always be included - not every publisher collects ORCID iDs.
   */
  ORCID?: string;
}
export interface CiteProcInstitution {
  literal: string;
}
export type CiteProcName = CiteProcPerson | CiteProcInstitution;
export function isCiteProcInstitution(person: CiteProcName): person is CiteProcInstitution {
  return typeof (person as CiteProcInstitution).literal !== 'undefined';
}
export function normaliseCiteProcName(person: CiteProcName): string {
  if (isCiteProcInstitution(person)) {
    return person.literal;
  }

  if (person.given) {
    return `${person.given} ${person.family}`;
  }

  return person.family;
}

/**
 * @param orcidId An ORCID ID, potentially formatted as a link
 * @returns The ORCID iD formatted as the naked ID, e.g. 0000-0001-7237-0797, or
 *          undefined if not an ORCID iD.
 */
export function normaliseOrcidId(orcidId?: string) {
  if (typeof orcidId === 'undefined') {
    return undefined;
  }

  // Remove something like `https://orcid.org/` when present:
  orcidId = orcidId.replace(/^(https?:\/\/(www\.)?)?orcid.org\//, '');

  if (/^\d{4}\-\d{4}\-\d{4}\-\d{3}(\d|X)$/.test(orcidId)) {
    return orcidId;
  }

  return undefined;
}

// See https://citeproc-js.readthedocs.io/en/latest/csl-json/markup.html#items
interface CiteProcItem {
  id: string;
  DOI: string;
  type: 'article-journal' | 'article' | 'book' | 'dataset' | string;
  title: string;
  author: CiteProcName[];
  issued: { 'date-parts': CiteProcDate };
  URL?: string;
  abstract?: string;
}

// See https://citeproc-js.readthedocs.io/en/latest/csl-json/markup.html
export type CslJson = CiteProcItem;

// See https://crosscite.org/docs.html
export async function fetchDoiMetadata(doi:string) {
  try {
    // Note: some example DOIs from different registration agencies can be found at
    //       https://www.doi.org/demos.html
    const response = await fetch(
      `https://doi.org/${doi}`,
      {
        headers: {
          // CrossRef, DataCite and mEDRA all supposedly support CSL-JSON, so that should cover most works:
          // https://crosscite.org/docs.html#sec-4
          Accept: 'application/vnd.citationstyles.csl+json',
        },
      },
    );

    const data: CslJson = await response.json();

    return data;
  } catch (e) {
    return e as Error;
  }
}
