import fetch, { Headers } from 'node-fetch';
import { Endorsement } from '../entity/Endorsement';

interface ExportResult {
  sentEndorsements: Endorsement[];
  crossrefIds: string[];
  error?: Error;
};

export async function sendEndorsementsToCrossRefEventData(endorsements: Endorsement[]): Promise<ExportResult> {
  // We send endorsements to CrossRef Event Data in sequence rather than in parallel, in order to
  // be able to recover gracefully from failed requests (i.e. to be able to resume the export later).
  const exportPromise = endorsements.reduce(
    async (promiseSoFar, endorsement) => {
      const exportSoFar = await promiseSoFar;
      const requestBody = convertEndorsementToCrossRefEvent(endorsement);

      try {
        await fetch(
          process.env.CROSSREF_URL + 'events',
          {
            method: 'POST',
            body: JSON.stringify(requestBody),
            headers: new Headers({
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${process.env.CROSSREF_CLIENT_SECRET}`,
            }),
          },
        );
      } catch (e) {
        exportSoFar.error = e;
        throw exportSoFar;
      }

      exportSoFar.sentEndorsements.push(endorsement);
      exportSoFar.crossrefIds.push(requestBody.id);
      return exportSoFar;
    },
    Promise.resolve({ sentEndorsements: [], crossrefIds: [] } as ExportResult),
  );

  try {
    const exportResult = await exportPromise;

    return exportResult;
  } catch (e) {
    return e as ExportResult & { error: Error };
  }
}

export function hasError(response: ExportResult): response is (ExportResult & { error: Error }) {
  return response.error instanceof Error;
}

function convertEndorsementToCrossRefEvent(endorsement: Endorsement) {
  const objectId = (endorsement.pid.substr(0, 'doi:'.length) === 'doi:')
    ? `https://doi.org/${endorsement.pid.substring('doi:'.length)}`
    : endorsement.pid;

  const body = {
    "license": "https://creativecommons.org/publicdomain/zero/1.0/",
    "obj_id": objectId,
    "source_token": process.env.CROSSREF_CLIENT_ID,
    "occurred_at": endorsement.createdAt.toISOString(),
    "subj_id": process.env.ORCID_URL + endorsement.user.orcid,
    "id": endorsement.uuid,
    "action": "add",
    "source_id": "plaudit",
    "relation_type_id": "recommends"
  };

  return body;
}
