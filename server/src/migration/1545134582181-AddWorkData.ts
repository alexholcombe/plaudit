import {MigrationInterface, QueryRunner} from "typeorm";

export class AddWorkData1545134582181 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        // Note that this migration adds a restriction on endorsements that existing endorsements do
        // not fulfill (namely that they are linked to work_data). Thus, it requires an empty
        // database, which is the case at the time of writing.
        await queryRunner.query(`CREATE TABLE "work_author" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "orcid" character varying, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL, "pid" character varying, CONSTRAINT "PK_3321ac83d18c9d76a3e6dc3887b" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "work_data" ("pid" character varying NOT NULL, "url" character varying NOT NULL, "title" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL, CONSTRAINT "PK_67d67dfce84ad92d7e5957cb97d" PRIMARY KEY ("pid"))`);
        await queryRunner.query(`ALTER TABLE "work_author" ADD CONSTRAINT "FK_c98ee656c39a3a43c980b151f35" FOREIGN KEY ("pid") REFERENCES "work_data"("pid") ON DELETE CASCADE`);
        await queryRunner.query(`ALTER TABLE "endorsement" ADD CONSTRAINT "FK_2c4d84c5cca7e0fc06d934ffefc" FOREIGN KEY ("pid") REFERENCES "work_data"("pid")`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "endorsement" DROP CONSTRAINT "FK_2c4d84c5cca7e0fc06d934ffefc"`);
        await queryRunner.query(`ALTER TABLE "work_author" DROP CONSTRAINT "FK_c98ee656c39a3a43c980b151f35"`);
        await queryRunner.query(`DROP TABLE "work_data"`);
        await queryRunner.query(`DROP TABLE "work_author"`);
    }

}
