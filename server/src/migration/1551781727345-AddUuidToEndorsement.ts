import {MigrationInterface, QueryRunner} from "typeorm";

export class AddUuidToEndorsement1551781727345 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "endorsement" ADD "uuid" uuid NOT NULL DEFAULT uuid_generate_v4()`);
        // The following endorsements were already made before we added a UUID column,
        // so we insert the UUIDs we sent to CrossRef:
        await queryRunner.query(`UPDATE "endorsement" SET "uuid" = 'bfebeb70-39c9-11e9-b00d-d3905e1925b5' WHERE id=38`);
        await queryRunner.query(`UPDATE "endorsement" SET "uuid" = 'bff7f960-39c9-11e9-b00d-d3905e1925b5' WHERE id=39`);
        await queryRunner.query(`UPDATE "endorsement" SET "uuid" = 'c0014830-39c9-11e9-b00d-d3905e1925b5' WHERE id=40`);
        await queryRunner.query(`UPDATE "endorsement" SET "uuid" = 'c00a48e0-39c9-11e9-b00d-d3905e1925b5' WHERE id=41`);
        await queryRunner.query(`UPDATE "endorsement" SET "uuid" = 'c0128640-39c9-11e9-b00d-d3905e1925b5' WHERE id=42`);
        await queryRunner.query(`UPDATE "endorsement" SET "uuid" = 'c01ac3a0-39c9-11e9-b00d-d3905e1925b5' WHERE id=43`);
        await queryRunner.query(`UPDATE "endorsement" SET "uuid" = 'c024fcd0-39c9-11e9-b00d-d3905e1925b5' WHERE id=44`);
        await queryRunner.query(`UPDATE "endorsement" SET "uuid" = 'c031f520-39c9-11e9-b00d-d3905e1925b5' WHERE id=45`);
        await queryRunner.query(`UPDATE "endorsement" SET "uuid" = 'bfac4a60-39c9-11e9-b00d-d3905e1925b5' WHERE id=46`);
        await queryRunner.query(`UPDATE "endorsement" SET "uuid" = 'bfb9def0-39c9-11e9-b00d-d3905e1925b5' WHERE id=47`);
        await queryRunner.query(`UPDATE "endorsement" SET "uuid" = 'bfc68920-39c9-11e9-b00d-d3905e1925b5' WHERE id=48`);
        await queryRunner.query(`UPDATE "endorsement" SET "uuid" = 'bfcf62c0-39c9-11e9-b00d-d3905e1925b5' WHERE id=49`);
        await queryRunner.query(`UPDATE "endorsement" SET "uuid" = 'bfd88a80-39c9-11e9-b00d-d3905e1925b5' WHERE id=50`);
        await queryRunner.query(`UPDATE "endorsement" SET "uuid" = 'bfe311d0-39c9-11e9-b00d-d3905e1925b5' WHERE id=51`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "endorsement" DROP COLUMN "uuid"`);
    }

}
