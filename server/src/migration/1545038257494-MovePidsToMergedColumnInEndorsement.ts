import {MigrationInterface, QueryRunner} from "typeorm";

export class MovePidsToMergedColumnInEndorsement1545038257494 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "endorsement" ADD "pid" character varying`);
        // Set the value of the pid column to a concatenation of identifierType and identifier
        await queryRunner.query(`UPDATE "endorsement" SET pid="identifierType" || ':' || "identifier"`);
        await queryRunner.query(`DROP INDEX "IDX_2331ad9ebcab0bfcb34f38f880"`);
        await queryRunner.query(`ALTER TABLE "endorsement" DROP COLUMN "identifierType"`);
        await queryRunner.query(`ALTER TABLE "endorsement" DROP COLUMN "identifier"`);
        await queryRunner.query(`ALTER TABLE "endorsement" ALTER COLUMN "pid" SET NOT NULL`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_f2657d40873833f9ad22693f04" ON "endorsement"  ("pid", "userOrcid") `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP INDEX "IDX_f2657d40873833f9ad22693f04"`);
        await queryRunner.query(`ALTER TABLE "endorsement" ADD "identifier" character varying`);
        await queryRunner.query(`ALTER TABLE "endorsement" ADD "identifierType" character varying`);
        // Restore the identifierType and identifier columns by splitting the pid column
        await queryRunner.query(`UPDATE "endorsement" SET "identifierType"=split_part(pid, ':', 1), "identifier"=split_part(pid, ':', 2);`);
        await queryRunner.query(`ALTER TABLE "endorsement" ALTER COLUMN "identifierType" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "endorsement" ALTER COLUMN "identifier" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "endorsement" ALTER COLUMN "pid" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "endorsement" DROP COLUMN "pid"`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_2331ad9ebcab0bfcb34f38f880" ON "endorsement"  ("identifierType", "identifier", "userOrcid") `);
    }

}
