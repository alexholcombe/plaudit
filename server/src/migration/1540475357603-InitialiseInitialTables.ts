import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialiseInitialTables1540475357603 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "user" ("orcid" character varying NOT NULL, "name" text, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL, CONSTRAINT "PK_795dc4df20b3f6dbdeeeda2e30e" PRIMARY KEY ("orcid"))`);
        await queryRunner.query(`CREATE TABLE "tag" ("id" SERIAL NOT NULL, "tag" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL, CONSTRAINT "UQ_9dbf61b2d00d2c77d3b5ced37c6" UNIQUE ("tag"), CONSTRAINT "PK_8e4052373c579afc1471f526760" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "endorsement" ("id" SERIAL NOT NULL, "identifierType" character varying NOT NULL, "identifier" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL, "userOrcid" character varying, CONSTRAINT "PK_582872d085fc60ae57495eb9491" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_2331ad9ebcab0bfcb34f38f880" ON "endorsement"  ("identifierType", "identifier", "userOrcid") `);
        await queryRunner.query(`CREATE TABLE "session" ("sid" character varying NOT NULL, "expires" TIMESTAMP WITH TIME ZONE, "data" json NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_7575923e18b495ed2307ae629ae" PRIMARY KEY ("sid"))`);
        await queryRunner.query(`CREATE TABLE "endorsement_tags_tag" ("endorsementId" integer NOT NULL, "tagId" integer NOT NULL, CONSTRAINT "PK_53f494b6b3554f3bba2b5379f6a" PRIMARY KEY ("endorsementId", "tagId"))`);
        await queryRunner.query(`ALTER TABLE "endorsement" ADD CONSTRAINT "FK_7af6ceac7a45024d76b255af4bb" FOREIGN KEY ("userOrcid") REFERENCES "user"("orcid") ON DELETE CASCADE`);
        await queryRunner.query(`ALTER TABLE "endorsement_tags_tag" ADD CONSTRAINT "FK_f50b2ddb59216e809b29f5d9ad3" FOREIGN KEY ("endorsementId") REFERENCES "endorsement"("id") ON DELETE CASCADE`);
        await queryRunner.query(`ALTER TABLE "endorsement_tags_tag" ADD CONSTRAINT "FK_9e2d63f85177a304c758485947c" FOREIGN KEY ("tagId") REFERENCES "tag"("id") ON DELETE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "endorsement_tags_tag" DROP CONSTRAINT "FK_9e2d63f85177a304c758485947c"`);
        await queryRunner.query(`ALTER TABLE "endorsement_tags_tag" DROP CONSTRAINT "FK_f50b2ddb59216e809b29f5d9ad3"`);
        await queryRunner.query(`ALTER TABLE "endorsement" DROP CONSTRAINT "FK_7af6ceac7a45024d76b255af4bb"`);
        await queryRunner.query(`DROP TABLE "endorsement_tags_tag"`);
        await queryRunner.query(`DROP TABLE "session"`);
        await queryRunner.query(`DROP INDEX "IDX_2331ad9ebcab0bfcb34f38f880"`);
        await queryRunner.query(`DROP TABLE "endorsement"`);
        await queryRunner.query(`DROP TABLE "tag"`);
        await queryRunner.query(`DROP TABLE "user"`);
    }

}
