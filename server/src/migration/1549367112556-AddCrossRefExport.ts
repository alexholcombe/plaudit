import {MigrationInterface, QueryRunner} from "typeorm";

export class AddCrossRefExport1549367112556 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "cross_ref_export" ("id" SERIAL NOT NULL, "exportedEndorsements" integer NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL, "exportedUntilId" integer, CONSTRAINT "PK_d0e50687e99cf2406a27996372d" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "cross_ref_export" ADD CONSTRAINT "FK_bd534ab422e9347521cc4ace3da" FOREIGN KEY ("exportedUntilId") REFERENCES "endorsement"("id")`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "cross_ref_export" DROP CONSTRAINT "FK_bd534ab422e9347521cc4ace3da"`);
        await queryRunner.query(`DROP TABLE "cross_ref_export"`);
    }

}
