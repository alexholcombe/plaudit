import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateEndorsementRequestLog1542884619646 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "endorsement_request" ("id" SERIAL NOT NULL, "pid" character varying NOT NULL, "integratorId" character varying, "referrer" character varying, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL, CONSTRAINT "PK_c4a0e64f2a46d48460748f77f03" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE "endorsement_request"`);
    }

}
