import { Store } from "express-session";
import * as express from 'express';
import { getConnection } from "typeorm";

import { Session } from "./entity/Session";

export function initialiseTypeOrmStore(BaseStore: typeof Store) {
  class TypeOrmStore extends BaseStore {
    regenerate: (req: express.Request, fn: (err?: any) => any) => void;
    load: (sid: string, fn: (err: any, session?: Express.SessionData | null) => any) => void;
    createSession: (req: express.Request, sess: Express.SessionData) => void;

    get: (
      sid: string,
      callback: (err: any, session?: Express.SessionData | null) => void,
    ) => void = async (sid, callback) => {
      try {
        const session = await getConnection()
          .getRepository(Session)
          .createQueryBuilder('session')
          .where('sid = :sid', { sid })
          .getOne();

        callback(null, (session) ? session.data : undefined);
      } catch(error) {
        callback(error);
      }
    };

    set: (
      sid: string,
      session: Express.SessionData,
      callback?: (err?: any) => void,
    ) => void = async (sid, sessionData, callback) => {
      callback = (typeof callback === 'function') ? callback : () => undefined;

      try {
        await getConnection()
          .createQueryBuilder()
          .insert()
          .into(Session)
          .values({
            sid,
            data: sessionData,
          })
          .onConflict('("sid") DO UPDATE SET "data" = :data')
          .setParameter('data', sessionData)
          .execute();
        callback(null);
      } catch(error) {
        callback(error);
      }
    };

    destroy: (
      sid: string,
      callback?: (err?: any) => void,
    ) => void = async (sid, callback) => {
      callback = (typeof callback === 'function') ? callback : () => undefined;

      try {
        await getConnection()
          .createQueryBuilder()
          .delete()
          .from(Session)
          .where('sid = :sid', { sid })
          .execute();

        callback();
      } catch(error) {
        callback(error);
      }
    };

    all: (callback: (err: any, obj?: { [sid: string]: Express.SessionData; } | null) => void) => void;
    length: (callback: (err: any, length?: number | null) => void) => void;
    clear: (callback?: (err?: any) => void) => void;
    touch: (sid: string, session: Express.SessionData, callback?: (err?: any) => void) => void;
  }

  return TypeOrmStore;
}
