import * as Passport from 'passport';
import * as OAuth2Strategy from 'passport-oauth2';
import { Express } from 'express';
import * as session from 'express-session';
import { getConnection } from 'typeorm';

import { initialiseTypeOrmStore } from '../typeorm-session-store';
import { User } from '../entity/User';

Passport.serializeUser((user: { orcid: string }, done) => done(null, user.orcid));
Passport.deserializeUser(async (orcid: string, done) => {
  try {
    const user = await getConnection()
      .getRepository(User)
      .createQueryBuilder('user')
      .where('user.orcid = :orcid', { orcid })
      .getOne();

    return done(null, user);
  } catch(e) {
    done(e);
  }
});

export const addAuth = async (app: Express) => {
  // We need to pass a scope to ORCID, which is an option we can pass to the strategy, even though
  // the types do not express this. I'm not sure what the correct types should look like, so for now
  // I simply manually extended it to allow for the `scope` option:
  const oAuthOptions: OAuth2Strategy.StrategyOptions & { scope?: string } = {
    authorizationURL: `${process.env.ORCID_URL}oauth/authorize`,
    tokenURL: `${process.env.ORCID_URL}oauth/token`,
    clientID: process.env.ORCID_CLIENT_ID!,
    clientSecret: process.env.ORCID_CLIENT_SECRET!,
    callbackURL: `${process.env.APP_URL}/api/orcid/callback`,
    scope: '/authenticate',
  };
  const storeUser: OAuth2Strategy.VerifyFunction = async (
    accessToken: string,
    refreshToken: string,
    params: {
      access_token: string;
      token_type: 'bearer';
      expires_in: number;
      scope: string;
      orcid: string;
      name?: string;
    },
    profile: {},
    callback: OAuth2Strategy.VerifyCallback,
  ) => {
    const user = {
      orcid: params.orcid,
      name: params.name,
    }

    try {
      await getConnection()
        .createQueryBuilder()
        .insert()
        .into(User)
        .values({
          orcid: user.orcid,
          name: user.name,
        })
        .onConflict('("orcid") DO UPDATE SET "name" = :name')
        .setParameter('name', user.name)
        .execute();
      callback(null, user);
    } catch(error) {
      callback(error);
    }
  };
  const strategy = new OAuth2Strategy(oAuthOptions, storeUser);
  Passport.use(strategy);

  const TypeOrmStore = initialiseTypeOrmStore(session.Store);
  const Store = new TypeOrmStore();
  app
  .use(session({
    store: Store,
    secret: process.env.SESSION_SECRET!,
    resave: false,
    saveUninitialized: true,
    cookie: {
      maxAge: 365 * 24 * 60 * 60 * 1000, // 1 year
      // secure: true,
      httpOnly: true,
    },
  }))
  .use(Passport.initialize())
  .use(Passport.session())
  .get('/api/orcid', Passport.authenticate('oauth2'))
  .get('/api/orcid/callback', Passport.authenticate('oauth2'), (req, res) => {
    // Tab that auto-closes to indicate that sign-in is complete:
    res.send(authCompleteHtml);
  });
};

const authCompleteHtml = `
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Signed in to Plaudit</title>
  <style>
body{
  margin:40px auto;
  max-width:650px;
  line-height:1.6;
  font-size:18px;
  color:#444;
  padding:0 10px;
}
  </style>
</head>
<body>
  <script>window.close();</script>
  <p>You are now signed in. Please close this page to complete the endorsement.</p>
</body>
</html>
`;
