import { Express } from 'express';
import fetch from 'node-fetch';
import * as cheerio from 'cheerio';

export const addProxy = async (app: Express) => {
  const baseUrl = (typeof process.env.APP_URL === 'string' && process.env.APP_URL !== 'https://www.plaudit.pub')
    ? process.env.APP_URL
    : 'https://plaudit.pub';
  const embedScript =`
    <script async src="${baseUrl}/embed/endorsements.js"
      data-embedder-id="plaudit"
    />
  `;

  app.get('/osf/:preprintId', async (req, res) => {
    if (req.params.preprintId) {
      // OSF Preprints executes Javascript to render the page, which we can't do when proxying (due
      // to security risks - browsers rightfully block it).
      // Luckily, they also do server-side rendering, so we can simply load that by appending
      // `?_escaped_fragment_=`.
      const response = await fetch(`https://osf.io/${req.params.preprintId}/?_escaped_fragment_=`);
      const html = (await response.text())
        // Stylesheet links on osf.io start with `/` - make sure they point to the original
        // resources here as well:
        .replace(/href="\//g, 'href="https://osf.io/');
      
      const $ = cheerio.load(html);

      const abstractDiv = $('.p-t-xs').first();
      abstractDiv.after(embedScript);

      res.send($.html());
    }
  });
};
