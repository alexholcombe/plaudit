import { Express } from 'express';
import { getConnection } from 'typeorm';

import { Endorsement as EndorsementModel } from '../entity/Endorsement';
import { CrossRefExport } from '../entity/CrossRefExport';
import { sendEndorsementsToCrossRefEventData, hasError } from '../data/crossref';

export const addCrossRefExport = async (app: Express) => {
  app.get('/api/export/crossref', async (req, res) => {
    const connection = getConnection();

    const latestExport = await connection
      .getRepository(CrossRefExport)
      .createQueryBuilder('export')
      .select()
      .where('export.exportedUntil IS NOT NULL')
      .leftJoinAndSelect('export.exportedUntil', 'exportedUntilEndorsement')
      .orderBy('export.id', 'DESC')
      .getOne();

    const newEndorsementsQuery = await connection
      .getRepository(EndorsementModel)
      .createQueryBuilder('endorsement')
      .select()
      .innerJoinAndSelect('endorsement.user', 'user');

    if (typeof latestExport !== 'undefined') {
      newEndorsementsQuery.where(
        'endorsement.id > :latestId',
        { latestId: latestExport.exportedUntil.id },
      );
    }

    const newEndorsements = await newEndorsementsQuery.getMany();
    const exportResults = await sendEndorsementsToCrossRefEventData(newEndorsements);
    const sentEndorsements = exportResults.sentEndorsements
      .sort((a, b) => a.id - b.id);

    await connection
      .createQueryBuilder()
      .insert()
      .into(CrossRefExport)
      .values({
        exportedEndorsements: sentEndorsements.length,
        exportedUntil:
          sentEndorsements[sentEndorsements.length - 1] ||
          (latestExport ? latestExport.exportedUntil : undefined),
      })
      .execute();

    if (hasError(exportResults)) {
      res.status(500);
    }
    res.json(exportResults);
  });
};
