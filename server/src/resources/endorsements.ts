import { Express } from 'express';
import * as bodyParser from 'body-parser';
import { getConnection } from 'typeorm';

import { Endorsement, GetEndorsementsResponse, PostEndorsementsRequest, PostEndorsementsResponse } from '../../../lib/endpoints';
import { Endorsement as EndorsementModel } from '../entity/Endorsement';
import { EndorsementRequest as EndorsementRequestModel } from '../entity/EndorsementRequest';
import { Tag } from '../entity/Tag';
import { WorkData } from '../entity/WorkData';
import { fetchDoiMetadata, normaliseCiteProcName, isCiteProcInstitution, normaliseOrcidId } from '../data/doi';
import { WorkAuthor } from '../entity/WorkAuthor';

  // A DOI is `10.`, followed by at least four digits, then a slash, and then any number of characters
const doiRegex = /^10\.\d{4,}\/.*$/;

export const addEndorsements = async (app: Express) => {
  app.get('/api/endorsements/*', async (req, res) => {
    if (typeof req.params[0] === 'undefined' || !doiRegex.test(req.params[0])) {
      res.sendStatus(400);
      return;
    }
    const doi = req.params[0];

    const queryParams: { embedder_id?: string, referrer?: string } = req.query;

    const connection = getConnection();

    const query = await connection
      .getRepository(WorkData)
      .createQueryBuilder('workData')
      .leftJoinAndSelect('workData.authors', 'authors')
      .leftJoinAndSelect('workData.endorsements', 'endorsements')
      .leftJoinAndSelect('endorsements.user', 'user')
      .where('workData.pid = :pid', { pid: `doi:${doi}` });

    const result = await query.getOne();

    let endorsements: Array<Endorsement>;
    let doiMetadata: {
      title: string;
      authors: Array<{ name: string, orcid?: string }>;
      url: string;
    };

    if (typeof result === 'undefined') {
      const fetchedMetadata = await fetchDoiMetadata(doi);
      if (fetchedMetadata instanceof Error) {
        return res.sendStatus(404);
      }
      doiMetadata = {
        authors: fetchedMetadata.author.map((author) => ({
          name: normaliseCiteProcName(author),
          orcid: !isCiteProcInstitution(author) ? author.ORCID : undefined,
        })),
        title: fetchedMetadata.title,
        url: fetchedMetadata.URL || `https://doi.org/${doi}`
      };
      // If no details about the work were present in the database, no one can have endorsed it either:
      endorsements = [];
    } else {
      doiMetadata = {
        authors: result.authors.map((author) => ({ name: author.name, orcid: author.orcid || undefined })),
        title: result.title,
        url: result.url,
      };
      endorsements = result.endorsements.map(row => ({
        doi: row.pid.replace(/^doi:/, ''),
        orcid: row.user.orcid,
        type: 'robust' as 'robust',
        userName: row.user.name,
      }));
    }

    const response: GetEndorsementsResponse = {
      endorsements,
      workData: doiMetadata,
      user: req.user
        ? { orcid: req.user.orcid, name: req.user.name }
        : undefined,
    };
    // Return them as json
    res.json(response);

    // Save this work's metadata to the database, if it wasn't present already
    if (typeof result === 'undefined') {
      connection.transaction(async (transactionalEntityManager) => {
        const dataToSave = new WorkData();
        dataToSave.pid = `doi:${doi}`;
        dataToSave.title = doiMetadata.title;
        dataToSave.url = doiMetadata.url;

        await transactionalEntityManager
          .createQueryBuilder()
          .insert()
          .into(WorkData)
          .values(dataToSave)
          .onConflict('("pid") DO NOTHING')
          .execute();

        await transactionalEntityManager
          .createQueryBuilder()
          .insert()
          .into(WorkAuthor)
          .values(doiMetadata.authors.map(author => ({
            name: author.name,
            workData: dataToSave,
            orcid: normaliseOrcidId(author.orcid),
          })))
          .execute();
      });
    }

    // Log that endorsements for this DOI were requested
    const endorsementRequest = new EndorsementRequestModel();
    endorsementRequest.pid = `doi:${doi}`;
    // Note: we don't want the actual Referer header here,
    // since that will be the location of the widget's iframe.
    // Instead, the embed script passes on its document.location as a query param.
    endorsementRequest.referrer = queryParams.referrer;
    endorsementRequest.integratorId = queryParams.embedder_id;
    await connection
      .getRepository(EndorsementRequestModel)
      .save(endorsementRequest);
  });

  app.post('/api/endorsements/*', bodyParser.json(), async (req, res) => {
    if (!req.user) {
      res.sendStatus(403);

      return;
    }

    const { orcid } = req.user;
    const doi = req.params[0];
    const identifierType = 'doi';

    const body: Partial<PostEndorsementsRequest> = req.body;

    if (!body.type || typeof doi !== 'string' || !doiRegex.test(doi)) {
      res.sendStatus(400);

      return;
    }

    const connection = getConnection();
    const results: EndorsementModel[] = await connection.transaction(async (transactionalEntityManager) => {
      const tags = ['robust'];
      await transactionalEntityManager
        .createQueryBuilder()
        .insert()
        .into(Tag)
        .values(tags.map(tag => ({ tag })))
        .onConflict('("tag") DO NOTHING')
        .execute();

      const endorsement = new EndorsementModel();
      endorsement.user = orcid;
      endorsement.pid = `${identifierType}:${doi}`;

      await transactionalEntityManager
        .createQueryBuilder()
        .insert()
        .into(EndorsementModel)
        .values(endorsement)
        .onConflict('("pid", "userOrcid") DO NOTHING')
        .returning('id')
        .execute();

      const tagRows = await transactionalEntityManager
        .getRepository(Tag)
        .createQueryBuilder()
        .where('tag IN (:...tags)', { tags })
        .getMany();

      const existingEndorsement = await transactionalEntityManager
        .getRepository(EndorsementModel)
        .findOneOrFail({
          relations: ['tags'],
          where: {
            user: orcid,
            pid: `${identifierType}:${doi}`,
          },
        });

      existingEndorsement.tags = tagRows;
      await transactionalEntityManager.save(existingEndorsement);

      const endorsements = transactionalEntityManager
        .getRepository(EndorsementModel)
        .createQueryBuilder('endorsement')
        .innerJoinAndSelect('endorsement.user', 'user')
        .where('endorsement.pid = :pid', { pid: `doi:${doi}` })
        .getMany();

      return endorsements;
    });

    const endorsements: Array<Endorsement> = results.map(row => ({
      doi: row.pid.replace(/^doi:/, ''),
      orcid: row.user.orcid,
      type: 'robust' as 'robust',
      userName: row.user.name,
    }));

    const response: PostEndorsementsResponse = {
      endorsements,
      user: { orcid: req.user.orcid, name: req.user.name },
    };

    res.json(response);
  });
};
