import { Express } from 'express';
import * as bodyParser from 'body-parser';
import { getConnection } from 'typeorm';

import { GetEndorsersResponse, GetEndorserResponse } from '../../../lib/endpoints';
import { Endorsement } from '../entity/Endorsement';
import { User } from '../entity/User';
import { Tag } from '../entity/Tag';
import { stripRssExtensions, isRssRequest, endorserToFeed, isAtomRequest } from '../data/toFeed';

export const addEndorsers = async (app: Express) => {
  app.get('/api/endorsers/', async (req, res) => {
    const connection = getConnection();

    const query = await connection
      .getRepository(User)
      .createQueryBuilder('user')
      .leftJoinAndSelect(
        'user.endorsements',
        'endorsement',
        'endorsement.createdAt > (CURRENT_DATE - INTERVAL \'1 month\')',
      )
      .groupBy('user.orcid')
      .addGroupBy('endorsement.id')
      .orderBy('endorsement.createdAt', 'DESC')
      .having('COUNT(endorsement.*) > 0');

    const results = await query.getMany();

    const endorsers = results.map((row) => ({
      orcid: row.orcid,
      name: row.name,
      endorsements: row.endorsements.map((endorsement) => ({
        identifierType: endorsement.pid.substring(0, endorsement.pid.indexOf(':')) as 'doi',
        identifier: endorsement.pid.substring(endorsement.pid.indexOf(':') + 1),
      })),
    }));

    const response: GetEndorsersResponse = {
      endorsers: endorsers,
    };

    res.json(response);
  });

  app.get('/api/endorsers/:orcid', async (req, res) => {
    // An ORCID iD consists of four groups of four digits,
    // with the caveat that the final digit can also be an X:
    const orcidRegex = /^\d{4}\-\d{4}\-\d{4}\-\d{3}(\d|X)$/;

    if (typeof req.params.orcid === 'undefined' || !orcidRegex.test(stripRssExtensions(req.params.orcid))) {
      res.sendStatus(400);
      return;
    }

    const orcid = stripRssExtensions(req.params.orcid);

    const connection = getConnection();

    const query = await connection
      .getRepository(User)
      .createQueryBuilder('user')
      .leftJoinAndSelect(
        'user.endorsements',
        'endorsement',
      )
      .leftJoinAndSelect(
        'endorsement.workData',
        'workData',
      )
      .where('user.orcid = :orcid', { orcid: orcid });

    const result = await query.getOne();

    if (typeof result === 'undefined') {
      return res.send(404);
    }

    const endorser = {
      orcid: result.orcid,
      name: result.name,
      endorsements: result.endorsements.map((endorsement) => ({
        identifierType: endorsement.pid.substring(0, endorsement.pid.indexOf(':')) as 'doi',
        identifier: endorsement.pid.substring(endorsement.pid.indexOf(':') + 1),
        title: endorsement.workData.title,
        url: endorsement.workData.url,
        timestamp: endorsement.createdAt.toISOString(),
      }))
    };

    const response: GetEndorserResponse = {
      endorser: endorser,
    };

    if (isAtomRequest(req)) {
      res.set('Content-Type', 'application/atom+xml');
      return res.send(endorserToFeed(response).atom1());
    }
    if (isRssRequest(req)) {
      res.set('Content-Type', 'application/rss+xml');
      return res.send(endorserToFeed(response).rss2());
    }
    res.json(response);
  });
};
