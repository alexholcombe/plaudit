import {
    Entity,
    Column,
    VersionColumn,
    UpdateDateColumn,
    CreateDateColumn,
    PrimaryColumn,
    OneToMany,
} from "typeorm";

import { Endorsement } from "./Endorsement";
import { WorkAuthor } from "./WorkAuthor";

@Entity()
export class WorkData {
    @PrimaryColumn()
    pid: string;

    @OneToMany(() => Endorsement, endorsement => endorsement.workData)
    endorsements: Endorsement[];

    @OneToMany(() => WorkAuthor, author => author.workData)
    authors: WorkAuthor[];

    @Column()
    url: string;

    @Column()
    title: string;

    @CreateDateColumn()
    public createdAt: Date;

    @UpdateDateColumn()
    public updatedAt: Date;

    @VersionColumn()
    public version: number;
}
