import {
    Entity,
    Column,
    VersionColumn,
    UpdateDateColumn,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
} from "typeorm";

import { Endorsement } from "./Endorsement";

@Entity()
export class CrossRefExport {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Endorsement, { nullable: true })
    exportedUntil: Endorsement;

    @Column()
    exportedEndorsements: number;

    @CreateDateColumn()
    public createdAt: Date;

    @UpdateDateColumn()
    public updatedAt: Date;

    @VersionColumn()
    public version: number;
}
