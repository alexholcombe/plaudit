import {
    Entity,
    Column,
    PrimaryColumn,
    UpdateDateColumn,
    CreateDateColumn,
} from "typeorm";

@Entity()
export class Session {
    @PrimaryColumn()
    sid: string;

    @Column({ type: 'timestamptz', nullable: true })
    expires: Date | undefined;

    @Column({ type: 'json' })
    data: Express.SessionData;

    @CreateDateColumn()
    public createdAt: Date;

    @UpdateDateColumn()
    public updatedAt: Date;
}
