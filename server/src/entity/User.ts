import {
    Entity,
    Column,
    PrimaryColumn,
    VersionColumn,
    UpdateDateColumn,
    CreateDateColumn,
    OneToMany,
} from "typeorm";
import { Endorsement } from "./Endorsement";

@Entity()
export class User {
    @PrimaryColumn()
    orcid: string;

    @Column({ type: 'text', nullable: true })
    name: undefined | string;

    @OneToMany(type => Endorsement, endorsement => endorsement.user)
    endorsements: Endorsement[];

    @CreateDateColumn()
    public createdAt: Date;

    @UpdateDateColumn()
    public updatedAt: Date;
  
    @VersionColumn()
    public version: number;
}
