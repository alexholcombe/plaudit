import {
    Entity,
    Column,
    VersionColumn,
    UpdateDateColumn,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    ManyToMany,
    JoinTable,
    Index,
    JoinColumn,
    Generated,
} from "typeorm";

import { User } from "./User";
import { Tag } from "./Tag";
import { WorkData } from "./WorkData";

@Entity()
@Index(['pid', 'user'], { unique: true })
export class Endorsement {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    pid: string;

    @ManyToOne(type => WorkData, workData => workData.endorsements)
    @JoinColumn({
        name: 'pid',
        referencedColumnName: 'pid',
    })
    workData: WorkData;

    @ManyToOne(type => User, user => user.orcid, { onDelete: 'CASCADE' })
    user: User;

    @ManyToMany(type => Tag, { cascade: true })
    @JoinTable()
    tags: Tag[];

    @Column()
    @Generated('uuid')
    uuid: string;

    @CreateDateColumn()
    public createdAt: Date;

    @UpdateDateColumn()
    public updatedAt: Date;

    @VersionColumn()
    public version: number;
}
