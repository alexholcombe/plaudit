import {
    Entity,
    Column,
    VersionColumn,
    UpdateDateColumn,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";

import { WorkData } from "./WorkData";

@Entity()
export class WorkAuthor {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => WorkData, workData => workData.authors, { onDelete: 'CASCADE' })
    @JoinColumn({
        name: 'pid',
        referencedColumnName: 'pid',
    })
    workData: WorkData;

    @Column()
    name: string;

    @Column({ nullable: true })
    orcid: string;

    @CreateDateColumn()
    public createdAt: Date;

    @UpdateDateColumn()
    public updatedAt: Date;

    @VersionColumn()
    public version: number;
}
