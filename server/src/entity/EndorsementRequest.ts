import {
    Entity,
    Column,
    VersionColumn,
    UpdateDateColumn,
    CreateDateColumn,
    PrimaryGeneratedColumn,
} from "typeorm";

@Entity()
export class EndorsementRequest {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    pid: string;

    @Column({ nullable: true })
    integratorId?: string;

    @Column({ nullable: true })
    referrer?: string;

    @CreateDateColumn()
    public createdAt: Date;

    @UpdateDateColumn()
    public updatedAt: Date;

    @VersionColumn()
    public version: number;
}
