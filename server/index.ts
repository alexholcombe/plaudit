const path = require('path');
import { URL } from 'url';

import * as express from 'express';
import { Connection } from 'typeorm';

import { addAuth } from './src/resources/auth';
import { addEndorsements } from './src/resources/endorsements';
import { addEndorsers } from './src/resources/endorsers';
import { initialiseDatabase } from './src/database';
import { addProxy } from './src/resources/proxy';
import { addCrossRefExport } from './src/resources/export';

const initialise = async () => {
  const connection: Connection | Error = await initialiseDatabase();

  if (connection instanceof Error) {
    console.error('Failed to initalise due to a database error', connection);

    return;
  }

  const app = express();

  await addAuth(app);
  addEndorsements(app);
  addEndorsers(app);
  addCrossRefExport(app);
  addProxy(app);

  if (process.env.NODE_ENV === 'production') {
    app.use('/storybook', express.static(path.join(__dirname, '../storybook'), { extensions: [ 'html' ] }));
    // /storybook used to be /components, so redirect. Over time, this can be removed:
    app.get('/components', (req, res) => {
      res.redirect(301, req.originalUrl.replace('/components', '/storybook'));
    });
    app.use(express.static(path.join(__dirname, '../website'), { extensions: [ 'html' ] }));
    app.use('/widget', (req, res, next) => {
      const staticHandler = express.static(path.join(__dirname, '../client'), { extensions: [ 'html' ] });

      const embedderDomains: { [ embedder_id: string ]: string[] } = {
        'library_carpentry': [
          'https://librarycarpentry.org',
        ],
        'osf_preprints': [
          'https://osf.io'
        ],
        'web_ext': [
          'https://osf.io',
          'https://www.essoar.org',
          'https://arxiv.org',
          'https://scipost.org',
          'https://f1000research.com',
          'https://wellcomeopenresearch.org',
          'https://gatesopenresearch.org',
          'https://mniopenresearch.org',
          'https://hrbopenresearch.org',
          'https://aasopenresearch.org',
          'https://amrcopenresearch.org',
          'https://isfopenresearch.org',
          'https://www.nature.com',
          'https://www.sciencedirect.com',
          'https://onlinelibrary.wiley.com',
          'https://*.onlinelibrary.wiley.com',
          'https://link.springer.com',
          'https://www.tandfonline.com',
          'https://academic.oup.com',
          'https://www.biorxiv.org',
          'https://www.preprints.org',
          'https://www.zenodo.org',
          'https://zenodo.org',
          'https://www.mdpi.com',
          'https://mdpi.com',
          'https://www.ncbi.nlm.nih.gov',
          'https://www.pnas.org',
          'moz-extension:',
          'chrome-extension:'
        ],
        'flockademic': [
          'https://flockademic.com',
          'https://www.flockademic.com',
          'https://dev.flockademic.com',
        ],
        'plaudit': [ 'https://plaudit.pub', process.env.APP_URL || 'https://www.plaudit.pub' ],
      };
      let frameAncestors = 'none';
      let frameOptions = 'deny';

      if (req.query && typeof req.query.embedder_id === 'string' && Array.isArray(embedderDomains[req.query.embedder_id])) {
        frameAncestors = embedderDomains[req.query.embedder_id].join(' ');

        // Note: the only reason we add the X-Frame-Options header is because Internet Explorer
        //       does not support the Content-Security-Policy header. Unfortunately, it does not
        //       seem to be possible to add multiple domains for that header, which is why we check
        //       the Referer header for the domain to whitelist here. BUT: not having access to
        //       an installation of Internet Explorer, I have not been able to test whether it
        //       actually sets this header to the URL of the page that includes the iframe.
        //       Therefore, if the widget does not work in Internet Explorer, it might be better to
        //       remove X-Frame-Options and just live without the embed protection there.
        const referrer = req.get('Referer');
        if (typeof referrer === 'string') {
          const referrerDomain = new URL(referrer).origin;
          const embedderToAllow = embedderDomains[req.query.embedder_id]
            .find((domain) => domain === referrerDomain);

          if (typeof embedderToAllow === 'string') {
            frameOptions = `allow-from ${embedderToAllow}`;
          }
        }
      }
      res.setHeader('Content-Security-Policy', `frame-ancestors ${frameAncestors}`);
      res.setHeader('X-Frame-Options', frameOptions);

      staticHandler(req, res, next);
    });
    app.use('/embed', express.static(path.join(__dirname, '../embed')));

    app.get('*', (req, res) => {
      res.sendFile(path.join(__dirname, '../website/404.html'));
    });
  }

  const port = process.env.PORT || 5000;
  app.listen(port, () => console.log('Server listening on port', port));
}

initialise();
