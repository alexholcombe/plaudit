const config = {
  "type": "postgres",
  "entities": ["src/entity/*"],
  "migrationsTableName": "migrations",
  "migrations": ["src/migration/*"],
  "cli": {
      "migrationsDir": "src/migration"
  }
}

if (process.env.DATABASE_URL) {
  config.url = process.env.DATABASE_URL;
} else {
  config.host = "localhost";
  config.port = 5432;
  config.username = "postgres";
  config.password = "";
  config.database = "plaudit_db";
}

if (process.env.NODE_ENV === 'development') {
  config.logging = ['query'];
}

module.exports = config;
