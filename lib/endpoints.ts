export type EndorsementType = 'robust';
export interface Endorsement {
  orcid: string;
  doi: string;
  type: EndorsementType;
}

export interface GetEndorsementsResponse {
  endorsements: Array<Endorsement & { userName?: string }>;
  workData: {
    title: string;
    url: string;
    authors: Array<{
      name: string;
      orcid?: string;
    }>;
  };
  user?: {
    name?: string;
    orcid: string;
  };
}

export interface GetEndorsersResponse {
  endorsers: Array<{
    orcid: string;
    name?: string;
    endorsements: Array<{
      identifierType: 'doi',
      identifier: string,
    }>;
  }>;
}

export interface GetEndorserResponse {
  endorser: {
    orcid: string;
    name?: string;
    endorsements: Array<{
      identifierType: 'doi',
      identifier: string,
      title: string,
      url: string,
      /** ISO 8601 timestamp */
      timestamp: string,
    }>;
  };
}

export interface PostEndorsementsRequest {
  type: EndorsementType;
}
export interface PostEndorsementsResponse {
  endorsements: Array<Endorsement & { userName?: string }>;
  user: {
    name?: string;
    orcid: string;
  };
}
